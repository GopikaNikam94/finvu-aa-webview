export enum ResponseStatus {
   Success = 'SUCCESS',
   Failure = 'FAILURE',
   Record_not_Found = 'RECORD_NOT_FOUND'
}

export enum RESPONSE_STATUS  {
   SEND = 'SEND',
   ACCEPT = 'ACCEPT',
   REJECT = 'REJECT',
   RETRY = 'RETRY',
   SUCCESS = 'SUCCESS',
   FAILURE = 'FAILURE',
   LOCKED = 'LOCKED',
   VERIFY = 'VERIFY',
   RECORD_NOT_FOUND = 'RECORD-NOT-FOUND' 
}

export enum DATA_LIFE_UNIT  {
   MONTH = 'MONTH(s)',
   YEAR = 'YEAR(s)',
   INF = 'FOREVER',
   DAY = 'DAY(s)'
}
    
export enum ACTIONS  {
   CONSENT = 'consent',
   SIGNUP = 'signup',
   LOGIN = 'login'
}


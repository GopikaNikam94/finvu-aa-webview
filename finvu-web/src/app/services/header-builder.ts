import { MsgHeader } from "./msg-header";
import * as uuid from 'uuid';

export class HeaderBuilder {
    public static build(mid: string, type: string): MsgHeader {

        if(mid === '' || mid == null){
            mid = uuid.v4();
        }

        if(type === '' || type == null){
            throw new TypeError("Message type not passed.");
        }

        const msgHeader: MsgHeader = {
            mid: mid,
            ts: new Date(),
            sid: sessionStorage.getItem("sid"),
            dup: false,
            type: type
        };
        return msgHeader;
    }
}
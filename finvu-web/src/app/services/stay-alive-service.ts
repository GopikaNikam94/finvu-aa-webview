import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class StayAliveRequestService {

    private SESSION_TIMEOUT_TIME: number = 15; interval

    constructor(private http: HttpClient, private router: Router) { }

    callTimeoutMethod() {
        var keepAlive = sessionStorage.getItem('keepAlive');
        var keepAliveTimeout: any = sessionStorage.getItem("keepAliveTimeOut");

        if (keepAlive) {
            this.interval = setInterval(() => {
                if (this.SESSION_TIMEOUT_TIME > 0) {
                    this.getUrl();
                    this.SESSION_TIMEOUT_TIME--;
                } else {
                    this.router.navigate(['onboarding/onboarding-error-page'])
                }
            }, keepAliveTimeout)
        }
    }

    getUrl() {
        console.log("check status");
        this.http.get(sessionStorage.getItem("keepAliveUrl")).subscribe(response => {
            console.log(response);
        }), catchError(this.handleError);
    }

    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
        } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        return throwError(errorMessage);
    }
}






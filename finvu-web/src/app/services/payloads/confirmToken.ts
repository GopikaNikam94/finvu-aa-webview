export interface ConfirmLinkingRequest {
    ver: String
    timestamp: Date
    txnid: String
    Accounts: CustomerAccounts[]
    token: String 
    AccountsLinkingRefNumber: String
    FIPDetails: AccountFIPDetails
}

export interface CustomerAccounts {
    accType: String
    accRefNumber: String
    maskedAccNumber: String
    FIType:String
}

export interface AccountFIPDetails {
    fipId: String
    fipName: String
}
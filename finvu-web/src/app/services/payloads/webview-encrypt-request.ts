export interface WebviewEncryptRequest {
    encryptedRequest: String
    requestDate: String
    encryptedFiuId: String
}

export interface EncryptRequest {
    errorCode: String
}

export interface WebviewOtpRequest {
    webViewEncryptedRequest: WebviewEncryptRequest
    mobileInitiationRequest: MobileInitiationRequest
}

export interface MobileInitiationRequest {
    mobileNum: string
}
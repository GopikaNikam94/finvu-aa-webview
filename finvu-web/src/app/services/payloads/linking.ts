export interface LinkingRequest {
    ver: String
    timestamp: Date
    txnid: String
    Customer:Customer
    FIPDetails:FIPDetails
}

export interface Customer {
    id: String
    Accounts: Array<Accounts>
}

export interface Accounts {
    accType: String
    accRefNumber: String
    maskedAccNumber: String
    FIType:String
}

export interface FIPDetails {
    fipId: String
    fipName: String
}
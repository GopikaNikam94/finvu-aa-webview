export interface EntityInfoRequest {
    entityId: string
    entityType: string
}

export enum EntityEnum {
    FIP = 'FIP',
    FIU = 'FIU'
}
export interface ForgotPasswordVerifyRequest {
    userId: String
    mobileNum: String,
    otp: String,
    newPassword: String
    confirmPassword: String
}

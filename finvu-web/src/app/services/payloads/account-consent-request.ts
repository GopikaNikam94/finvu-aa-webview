export interface AccountConsentRequest {
    AA:                            AA;
    consent:                       String;
    consentDetailDigitalSignature: String;
    consentTypes:                  String[];
    ConsentUse:                    ConsentUse;
    createTime:                    Date;
    DataDateTimeRange:             DataDateTimeRange;
    DataFilter:                    DataFilter[];
    DataLife:                      DataLife;
    expireTime:                    Date;
    fetchType:                     String;
    fiTypes:                       String[];
    FIPDetails:                    FIPDetail[];
    FIU:                           FIU;
    Frequency:                     Frequency;
    mode:                          String;
    Purpose:                       Purpose;
    Signature:                     String;
    startTime:                     Date;
    statusLastupdateTimestamp:     Date;
    txnid:                         String;
    User:                          User;
    ver:                           String; 
    consentHandleId:               String;
    handleStatus:                  String;
}

export interface AA {
    id: String;
}

export interface ConsentUse {
    logUri:          String;
    count:           number;
    lastUseDateTime: Date;
}

export interface DataDateTimeRange {
    from: Date;
    to:   Date;
}

export interface DataFilter {
    type:     String;
    operator: String;
    value:    String;
}

export interface DataLife {
    unit:  String;
    value: number;
}

export interface Frequency {
    unit:  String;
    value: number;
}

export interface FIPDetail {
    FIP:      FIP;
    Accounts: Account[];
}

export interface Account {
    fipId:                      String;
    maskedAccNumber:            String;
    accRefNumber:               String;
    linkRefNumber:              String; 
    FIType:                     String;
    accType:                    String;
}

export interface FIU {
    id:   String;
    name: String;
}

export interface FIP {
    id:   String;
    name: String;
}

export interface Purpose {
    code:     String;
    refUri:   String;
    text:     String;
    Category: Category;
}

export interface Category {
    type: String;
}

export interface User {
    id:      String;
    idTypes: String;
}


export interface AccountConsentRequestEncrypt {
    fiuId: String;
    handleStatus: String;
    consentHandleId: String;
    User: User;
    Accounts: Accounts[];
    ConsentUse: ConsentUse;
    encryptedConsentRequest: String;
}

export interface User {
    id:  String;
    idTypes: String;
}

export interface Accounts {
    fipId:             String;
    maskedAccNumber:   String;
    accRefNumber:      String;
    linkRefNumber:     String; 
    FIType:            String;
    accType:           String;
}

export interface ConsentUse {
    logUri:          String;
    count:           number;
    lastUseDateTime: Date;
}


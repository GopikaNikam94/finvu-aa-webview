import { Injectable } from '@angular/core';
import { HeaderBuilder } from './header-builder';
import { MT } from './message-types';
import { Message } from './message';
import { User, ConsentUse, DataDateTimeRange, DataFilter, DataLife, FIP, FIPDetail, Frequency, Category, 
    Purpose, AA, FIU, AccountConsentRequest } from 'src/app/services/payloads/account-consent-request';
import { AaWebsocketService } from './websocket/aa-websocket.service';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ResponseStatus } from './util';
import { UserOfflineMessageService } from './user-offline-message-service';

@Injectable({
    providedIn: 'root'
})

export class ConsentService {
   
    public consentRequestDetailsResponse: Observable<Message>;
    public consentRequestDetailsSubscription: Subscription;
    public consentResponse: Observable<Message>;
    public consentSubscription: Subscription;
    public responseStatus: String; 
    public responseMessage: String; 

    public accounts = [];
    public requestConsentId: String;
    public handleStatus: String;
    public fetchType: String;
    public consentTypes = [];
    public dateRanges: any;
    public consentRequestDetail:any;
    public fiTypes: any;
    public aaId: any
    public frequency: any;
    public dataLife: any;
    public aa: any;
    public fiu: any;
    public purpose: any;
    public category: any;
    public fiuName: any;
    public fromDate: any;
    public toDate: any;
    public fipName: any;
    public fipId: any;

    constructor(public aaWebSocket: AaWebsocketService, public router: Router, 
              public userOfflineMessageService: UserOfflineMessageService) {}

    consentRequest(status, consentRequestDetail, accounts) {
      
      console.log("accounts: "+ JSON.stringify(accounts));

      this.consentResponse = this.aaWebSocket.on<Message>(MT.RES.ACCOUNTCONSENTREQUEST);
      if(sessionStorage.getItem("sid") != null ) {
     
        console.log("consentRequestDetail1"+ JSON.stringify(consentRequestDetail))
        this.fetchType = consentRequestDetail.fetchType;
        this.consentTypes = consentRequestDetail.consentTypes;
        this.dateRanges = consentRequestDetail.DataDateTimeRange;
        this.fiTypes = consentRequestDetail.fiTypes
        this.frequency = consentRequestDetail.Frequency
        this.dataLife = consentRequestDetail.DataLife
        this.aa = consentRequestDetail.AA
        this.fiu = consentRequestDetail.FIU
        this.purpose = consentRequestDetail.Purpose
        this.category = this.purpose.Category

        const user: User = {
          id: sessionStorage.getItem("username"),
          idTypes: "FIU"
        }
      
        const consentUse: ConsentUse = {
          logUri: "consent_use_loguri",
          count: 2,
          lastUseDateTime: new Date()
        }
      
        const dataDateTimeRange: DataDateTimeRange = {
          from: this.dateRanges.from,
          to: this.dateRanges.to  
        }

        const dataFilter: DataFilter = {
            type: 'TRANSACTIONAMOUNT',  
            operator: '>',
            value:    '6'
        }
        
        const dataLife: DataLife = {
          unit:  this.dataLife.unit,
          value: this.dataLife.value
        }

        const frequency: Frequency = {
          unit:  this.frequency.unit,
          value: this.frequency.value
        }

        const category: Category = {
          type: this.category.type
        }

        const purpose: Purpose = {
          code:     this.purpose.code,
          refUri:   this.purpose.refUri,
          text:     this.purpose.text,
          Category: category
        }
      
        const aa : AA = {
        id: this.aa.id
        } 

        const fiu: FIU = {
          id: this.fiu.id,
          name: this.fiu.name
        }  
        
        const accountConsentRequest: AccountConsentRequest = {
          AA:                             aa,
          consent:                        "Y",
          consentDetailDigitalSignature:  "Consent Digital Signature",
          consentTypes:                   this.consentTypes,
          ConsentUse:                     consentUse,
          createTime:                     new Date(),
          DataDateTimeRange:              dataDateTimeRange,
          DataFilter:                     [dataFilter],
          DataLife:                       dataLife,
          expireTime:                     consentRequestDetail.expireTime,
          fetchType:                      this.fetchType,
          fiTypes:                        this.fiTypes,
          FIPDetails:                     accounts,
          FIU:                            fiu,
          Frequency:                      frequency,
          mode:                           consentRequestDetail.mode,
          Purpose:                        purpose,
          Signature:                      'Signature',
          startTime:                      new Date(),
          statusLastupdateTimestamp:      new Date(),
          txnid:                          consentRequestDetail.txnid,
          User:                           user,
          ver:                            consentRequestDetail.ver,
          consentHandleId:                sessionStorage.getItem("requestConsentId"),
          handleStatus:                   status
        }
      
        const msgHeader = HeaderBuilder.build(null, MT.REQ.ACCOUNTCONSENTREQUEST);
      
        const consentRequestMessage: Message = {
          header: msgHeader,
          payload: accountConsentRequest
        };
        console.log("Consent Request: ", JSON.stringify(consentRequestMessage));
        this.aaWebSocket.send(consentRequestMessage);
      
        this.consentSubscription = this.consentResponse.subscribe(value => {
          console.log("Consent Response: " + JSON.stringify(value));
          this.consentSubscription.unsubscribe();
          this.responseStatus = value.payload["status"] ;
          this.responseMessage = value.payload["message"] ;
          if (this.responseStatus == ResponseStatus.Success) {
            console.log(this.responseMessage)
            this.userOfflineMessageService.updateUserOffineMessageAck();
          } else if (this.responseStatus == ResponseStatus.Failure) {
            console.log(this.responseMessage)
            this.userOfflineMessageService.updateUserOffineMessageAck();
          } else if (this.responseStatus == ResponseStatus.Record_not_Found) {
            console.log(value.payload["message"])
          } else {
              alert(this.responseMessage)
          }
        });
      } else {
          this.router.navigate(['onboarding']);
          console.log("Session id not generated");
      }
  }
        
}
   
    



import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AaWebsocketService } from './aa-websocket.service';


@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [],
    providers: [
        AaWebsocketService
    ]
})
export class AaWebsocketModule {
}
import { Observable } from 'rxjs';
import { Message } from '../message';

export interface IAaWebsocketService {
    on<T>(event: string): Observable<Message>;
    send(data: Message): void;
    status: Observable<boolean>;
}
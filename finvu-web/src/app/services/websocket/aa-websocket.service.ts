import { Injectable, OnDestroy } from '@angular/core';
import { Observable, SubscriptionLike, Subject, Observer } from 'rxjs';
import { filter } from 'rxjs/operators';
import { WebSocketSubject, WebSocketSubjectConfig } from 'rxjs/webSocket';
import { share, distinctUntilChanged } from 'rxjs/operators';
import { IAaWebsocketService } from './aa-websocket.interfaces';
import { Message } from '../message';
import { ConfigService } from '../config/config-service';
import { SimpleService } from '../simple-service';

@Injectable({
  providedIn: 'root'
})
export class AaWebsocketService implements IAaWebsocketService, OnDestroy {

  private config: WebSocketSubjectConfig<Message>;

  private websocketSub: SubscriptionLike;
  private statusSub: SubscriptionLike;

  private websocket$: WebSocketSubject<Message>;
  private connection$: Observer<boolean>;
  private wsMessages$: Subject<Message>;

  public status: Observable<boolean>;

  public socket: WebSocketSubject<Message>;
  private isConnected: boolean;
  hostName: string; url: string;

  constructor(public configService: ConfigService, private simpleService: SimpleService) {
    this.wsMessages$ = new Subject<Message>();

    this.hostName = this.simpleService.getHostname();
    console.log(this.hostName)    
    this.url = this.hostName == 'localhost' ? configService.webSocketBaseUrl : "wss://" + this.hostName + configService.webSocketBaseUrl
    console.log(this.url)

    // connection status
    this.status = new Observable<boolean>((observer) => {
      this.connection$ = observer;
    }).pipe(share(), distinctUntilChanged());

    // run reconnect if not connection
    this.statusSub = this.status
        .subscribe((isConnected) => {
            this.isConnected = isConnected;
    });
    
    this.config = {
        url: this.url,
        closeObserver: {
            next: (event: CloseEvent) => {
                this.websocket$ = null;
                this.connection$.next(false);
            }
        },
        openObserver: {
            next: (event: Event) => {
                console.log('WebSocket connected!');
                this.connection$.next(true);
            }
        }
    };

    this.websocketSub = this.wsMessages$.subscribe(
        null, (error: ErrorEvent) => console.error('WebSocket error!', error)
    );

    this.connect();
  }

  ngOnDestroy() {
    this.websocketSub.unsubscribe();
    this.statusSub.unsubscribe();
  }

    /*
    * connect to WebSocket
    * */
  private connect(): void {
    this.websocket$ = new WebSocketSubject(this.config);

    this.websocket$.subscribe(
        (message) => this.wsMessages$.next(message),
        (error: Event) => {
            if (!this.websocket$) {
                console.error(error);
            }
    });
  }

  /*
   * on message event
   * */
  public on<T>(event: string): Observable<Message> {
    if (event) {
      return this.wsMessages$.pipe(
        filter((message: Message) => message.header.type === event));
    }
  }

  /*
   * on message to server
   * */
  public send(data: Message): void {
      this.websocket$.next(data);
  }
}

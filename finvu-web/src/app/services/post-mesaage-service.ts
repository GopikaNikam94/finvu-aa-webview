import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class PostMesaageService {

    postMessage(action, status) {
        var obj = { action: action, status: status, source: "finvu" };
        var myParent: Window = window.top;
        if (window.parent != window.top) {
            myParent = window.parent;
        }
        myParent.postMessage(obj, '*');
    }
}
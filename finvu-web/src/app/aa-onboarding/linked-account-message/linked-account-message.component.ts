import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-linked-account-message',
  templateUrl: './linked-account-message.component.html',
  styleUrls: ['./linked-account-message.component.css',
              '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class LinkedAccountMessageComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {}

  onOkClick(){
    this.router.navigate(['onboarding/select-bank']);
  }

  addLaterClick(){
    this.router.navigate(['onboarding/webview-login']);
  }
}

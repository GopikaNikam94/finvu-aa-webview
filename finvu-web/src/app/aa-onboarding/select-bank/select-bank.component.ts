import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Message } from 'src/app/services/message';
import { MT } from 'src/app/services/message-types';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { FipSearchRequest } from 'src/app/services/payloads/fip-search';
import { map, startWith } from 'rxjs/operators';
import { PopularSearchRequest } from 'src/app/services/payloads/popular-search';
import { UserInfo } from 'src/app/services/payloads/user-info';
import { RESPONSE_STATUS } from 'src/app/services/util';
import { ConfigService } from 'src/app/services/config/config-service';

@Component({
  selector: 'app-select-bank',
  templateUrl: './select-bank.component.html',
  styleUrls: ['./select-bank.component.css',
              '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class SelectBankComponent implements OnInit {

  public popularSearchResponse: Observable<Message>; userInfoResponse: Observable<Message>;
  public fipSearchResponse: Observable<Message>;
  public subscriptionPopularSearch: Subscription; userInfoSubscription: Subscription;
  subscriptionFipSearch: Subscription;
  public selectForm: FormGroup;
  public bankList: any; responseStatus: String; responseMessage: String; mobile: String; handleId: string;
  public fipNames = []; fipNamesList: any[]; popularSearchList = []; popularSearchImagesList = [];

  control = new FormControl();
  filteredFips: Observable<string[]>;

  constructor(private router:Router, private aaWebSocket: AaWebsocketService,
              private formBuilder: FormBuilder,  private configService: ConfigService) {}

  ngOnInit() {
    sessionStorage.removeItem("fipId");
    sessionStorage.removeItem("fipName");
    this.handleId = this.configService.userPostfixName;

    this.selectForm = this.formBuilder.group({
      fipList: ['']
    });
   this.userInfoRequest();
   this.popularSearch();
  }

  fetchFIP(values) {
    if(sessionStorage.getItem("sid") != null) {
      if(values != "") {
        this.fipSearch(values);
      } else {
        console.log("FIP name is empty")
      }
    }  else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }


  userInfoRequest() {
    if (sessionStorage.getItem('sid') != null) {
      this.userInfoResponse = this.aaWebSocket.on<Message>(MT.RES.USERINFO);

      var username;
      if (sessionStorage.getItem("username").includes("@finvu")){
        username  = sessionStorage.getItem("username")
      } else {
        username  = sessionStorage.getItem("username")+this.handleId;
      }
      const userInfoPayload: UserInfo = {
        userId: username,
      };

      const msgHeader = HeaderBuilder.build(null, MT.REQ.USERINFO);

      const userInfoMessage: Message = {
        header: msgHeader,
        payload: userInfoPayload
      };

      console.log("User Info Request :- " + JSON.stringify(userInfoMessage));
      this.aaWebSocket.send(userInfoMessage);
      this.userInfoSubscription = this.userInfoResponse.subscribe(value => {
        console.log("UserInfo Response :- " + JSON.stringify(value));
        this.userInfoSubscription.unsubscribe();
        this.responseStatus = value.payload["status"];
        this.responseMessage = value.payload["message"];

        if (this.responseStatus == RESPONSE_STATUS.ACCEPT) {
          var result = value.payload["UserInfo"];
          this.mobile = result.mobileNo.substring(6,10)
          console.log(this.mobile.substring(6,10))
        } else if (this.responseStatus == RESPONSE_STATUS.REJECT || RESPONSE_STATUS.RETRY) {
          alert(this.responseMessage)
        } else {
          alert(this.responseMessage)
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  popularSearch() {
    this.popularSearchResponse = this.aaWebSocket.on<Message>(MT.RES.POPULARSEARCH);
    const popularSearchRequestPayload: PopularSearchRequest = {};
    const msgHeader = HeaderBuilder.build(null, MT.REQ.POPULARSEARCH);

    const popularSearchRequestMessage: Message = {
      header: msgHeader,
      payload: popularSearchRequestPayload
    };
      
    this.aaWebSocket.send(popularSearchRequestMessage);

    this.subscriptionPopularSearch = this.popularSearchResponse.subscribe(value => {
      this.subscriptionPopularSearch.unsubscribe();
      var result = value.payload;
      this.popularSearchList = result['poularSearch']

      this.popularSearchList.forEach(element => {
        var icon = element['productIconUri'] == null ? 'assets/images/bank_large_light.png' : element['productIconUri'].replace('www.', '')  
          this.popularSearchImagesList.push({
            fipId: element.fipId,
            productName: element.productName,
            fipFitypes: element.fipFitypes,
            fipFsr: element.fipFsr,
            productDesc: element.productDesc,
            productIconUri: icon
          })
      });
    });
  }

  listClick(event: any, item: any) {
    console.log(item);
    sessionStorage.setItem("fipId",item.fipId);
    sessionStorage.setItem("fipName",item.productName);
    sessionStorage.setItem("fipFitypes", JSON.stringify(item.fipFitypes));
    this.router.navigate(['onboarding/discover']);
  }

  fipSearch(fipName) {    
    this.fipSearchResponse = this.aaWebSocket.on<Message>(MT.RES.FIPSEARCH);
    
    const fipSearchRequestPayload: FipSearchRequest = {
      fipOrProductName: fipName
    };

    const msgHeader = HeaderBuilder.build(null, MT.REQ.FIPSEARCH);

    const fipSearchRequestMessage: Message = {
      header: msgHeader,
      payload: fipSearchRequestPayload
    };
      
    this.aaWebSocket.send(fipSearchRequestMessage);

    this.subscriptionFipSearch = this.fipSearchResponse.subscribe(value => {
      this.subscriptionFipSearch.unsubscribe();
      this.bankList = value.payload;
      this.fipNamesList = this.bankList['FIPs'];
      if(this.fipNamesList.length == 0){
        console.log("FipId is not found");
      } else {
        this.fipNamesList.map(item => {
          return {
            fipName: item.fipName
          }
        }).forEach(item => this.fipNames.push(item.fipName));  
        
        sessionStorage.setItem("fipId",this.fipNamesList[0]['fipId']);
        sessionStorage.setItem("fipName",this.fipNamesList[0]['fipName']);
        sessionStorage.setItem("fipFitypes", JSON.stringify(this.fipNamesList[0]['FITypes']));
      }
    });
  }
  
  private _filter(value: string): string[] {
    const filterValue = this._normalizeValue(value);
    return this.fipNames.filter(fipName => this._normalizeValue(fipName).includes(filterValue));
  }

  private _normalizeValue(value: string): string {
    return value.toLowerCase().replace(/\s/g, '');
  }
  
  onNextClick() {
    if(this.selectForm.get('fipList').value == null || this.selectForm.get('fipList').value == "") {
      alert("Type bank name or select from popular institution")
    } else {
      this.fipSearch(this.selectForm.get('fipList').value);
      this.router.navigate(['onboarding/discover']);
    }
  }

  onKey(event: any) { 
    if(event.target.value == 'undefined' || event.target.value == null) {
      console.log("FIP not found")
    } else {
      console.log(event.target.value);
      this.fetchFIP(event.target.value);
      this.filteredFips = this.control.valueChanges.pipe(startWith(''),map(value => this._filter(value)));
    } 
  }
}

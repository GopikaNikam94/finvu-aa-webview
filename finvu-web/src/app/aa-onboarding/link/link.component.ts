import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from 'src/app/services/message';
import { Observable, Subscription, timer } from 'rxjs';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { MT } from 'src/app/services/message-types';
import { HeaderBuilder } from 'src/app/services/header-builder';
import * as uuid from 'uuid';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { LinkingRequest, Customer, FIPDetails } from 'src/app/services/payloads/linking';
import { UserInfo } from 'src/app/services/payloads/user-info';
import { AccountFIPDetails, ConfirmLinkingRequest } from 'src/app/services/payloads/confirmToken';
import { ConfigService } from 'src/app/services/config/config-service';
import { RESPONSE_STATUS } from 'src/app/services/util';
import { take, map } from 'rxjs/operators';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.css',
    '../../../assets/icofont/css/aa-onboarding-common.scss']
})

export class LinkComponent implements OnInit {

  public userInfoResponse: Observable<Message>; linkingResponse: Observable<Message>;
  confirmLinkingResponse: Observable<Message>;
  public userInfoSubscription: Subscription; linkingSubscription: Subscription;
  confirmLinkingSubscription: Subscription; subscriptionTimer: Subscription;
  public linkingForm: FormGroup;
  public responseStatus: String; responseMessage: String; userId: String; linkResponseStatus: String;
  mobile: String; mobileNo: String; fipName: string; version: string;
  public userInfo: any;
  public submitted = false; showlink = true;
  public accounts = []; linkAccounts = [];
  public counter$: Observable<number>; counterTime$: Observable<number>;
  public count = 15;

  constructor(public router: Router, public aaWebSocket: AaWebsocketService,
    public formBuilder: FormBuilder, public configService: ConfigService) {
    this.version = this.configService.version;
    this.linkingForm = formBuilder.group({
      'otp': [null, Validators.compose([Validators.required, this.otpLengthValidator, Validators.pattern('^[0-9]+$')])]
    });
  }

  ngOnInit() {
    this.fipName = sessionStorage.getItem("fipName");
    this.userInfoResponse = this.aaWebSocket.on<Message>(MT.RES.USERINFO);
    this.linkingResponse = this.aaWebSocket.on<Message>(MT.RES.ACCLINKING);
    this.confirmLinkingResponse = this.aaWebSocket.on<Message>(MT.RES.CONFIRMTOKEN);

    this.setTimer();
    
    if (sessionStorage.getItem("sid") != null) {
      this.userInfoRequest();
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
    this.accounts = JSON.parse(sessionStorage.getItem("accounts"));
    this.accounts.map(item => {
      return {
        maskedAccNumber: item.maskedAccNumber,
        accRefNumber: item.accRefNumber,
        FIType: item.FIType,
        accType: item.accType,
        productIconUri: item.productIconUri
      }
    }).forEach(item => this.linkAccounts.push(item));

    sessionStorage.setItem("linkedAccounts", JSON.stringify(this.accounts));
  }

  userInfoRequest() {

    const userInfoPayload: UserInfo = {
      userId: sessionStorage.getItem("username")
    };

    const msgHeader = HeaderBuilder.build(null, MT.REQ.USERINFO);

    const userInfoMessage: Message = {
      header: msgHeader,
      payload: userInfoPayload
    };

    console.log("User Info Request :- " + JSON.stringify(userInfoMessage));
    this.aaWebSocket.send(userInfoMessage);
    this.userInfoSubscription = this.userInfoResponse.subscribe(value => {
      console.log("UserInfo Response :- " + JSON.stringify(value));
      this.userInfoSubscription.unsubscribe();
      this.responseStatus = value.payload["status"];
      this.responseMessage = value.payload["message"];

      if (this.responseStatus == RESPONSE_STATUS.ACCEPT) {
        this.userInfo = value.payload["UserInfo"];
        this.mobile = this.userInfo.mobileNo
        console.log("Mobile: ", this.mobile)
        this.mobile = this.mobile.substring(6, 10)
        console.log("Mobile: ", this.mobile)
        this.userId = this.userInfo.userId
        console.log("UserId: ", this.userId)
        this.callAccountLinking();
      } else if (this.responseStatus == RESPONSE_STATUS.RETRY) {
        alert(this.responseMessage)
      } else if (this.responseStatus == RESPONSE_STATUS.REJECT) {
        alert(value.payload["message"])
      } else {
        alert(this.responseMessage)
      }
    });
  }

  private callAccountLinking() {

    if (sessionStorage.getItem("sid") != null) {
      const fipDetails: FIPDetails = {
        fipId: sessionStorage.getItem("fipId"),
        fipName: sessionStorage.getItem("fipName")
      }

      const customer: Customer = {
        id: this.userId,
        Accounts: this.linkAccounts
      }

      const linkingRequestPayload: LinkingRequest = {
        ver: this.version,
        timestamp: new Date(),
        txnid: uuid.v4(),
        FIPDetails: fipDetails,
        Customer: customer
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.ACCLINKING);

      const linkingRequestMessage: Message = {
        header: msgHeader,
        payload: linkingRequestPayload
      };

      console.log("linking Request :- " + JSON.stringify(linkingRequestMessage));
      this.aaWebSocket.send(linkingRequestMessage);


      this.linkingSubscription = this.linkingResponse.subscribe(value => {
        console.log("Linking Response :- " + JSON.stringify(value));
        sessionStorage.setItem("sid", value.header.sid);
        this.linkingSubscription.unsubscribe();
        this.responseStatus = value.payload["status"];
        this.responseMessage = value.payload["message"];

        if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
          sessionStorage.setItem("refNumber", value.payload["RefNumber"])
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  callConfirmLinking() {

    const fipDetails: AccountFIPDetails = {
      fipId: sessionStorage.getItem("fipId"),
      fipName: sessionStorage.getItem("fipName")
    }

    const confirmTokenRequestPayload: ConfirmLinkingRequest = {
      ver: this.version,
      timestamp: new Date(),
      txnid: uuid.v4(),
      AccountsLinkingRefNumber: sessionStorage.getItem("refNumber"),
      token: this.linkingForm.get('otp').value,
      Accounts: this.linkAccounts,
      FIPDetails: fipDetails
    }

    const msgHeader = HeaderBuilder.build(null, MT.REQ.CONFIRMTOKEN);

    const confirmTokenRequestMessage: Message = {
      header: msgHeader,
      payload: confirmTokenRequestPayload
    };

    console.log("Confirm Token Request :- " + JSON.stringify(confirmTokenRequestMessage));
    this.aaWebSocket.send(confirmTokenRequestMessage);

    this.confirmLinkingSubscription = this.confirmLinkingResponse.subscribe(value => {

      console.log("Confirm token Response :- " + JSON.stringify(value));
      this.confirmLinkingSubscription.unsubscribe();

      this.responseStatus = value.payload["status"];
      this.responseMessage = value.payload["message"];

      if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
        console.log(value.payload["message"]);
        this.router.navigate(['onboarding/linking-result']);
      } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
        this.linkingForm.get("otp").reset();
        alert(this.responseMessage)
      } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
        this.linkingForm.get("otp").reset();
        alert(this.responseMessage)
      } else {
        alert("Something went wrong.")
      }
    });
  }

  public setTimer() {
    this.count = 15;
    this.counterTime$ = timer(0, 1000).pipe(
      take(this.count),
      map(() => --this.count)
    );

    this.showlink = true;
    this.counter$ = timer(15000);
    this.subscriptionTimer = this.counter$.subscribe(() => {
      this.showlink = false;
    });
  }

  onNextClick() {
    if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
      this.callConfirmLinking();
    } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
      console.log(this.responseMessage);
      this.linkingForm.get("otp").reset();
      alert(this.responseMessage);
    } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
      this.linkingForm.get("otp").reset();
      alert(this.responseMessage)
    } else {
      alert("Something went wrong.")
    }
  }

  submitForm() {
    this.markFormTouched(this.linkingForm);
    if (this.linkingForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.linkingForm.getRawValue;
      this.onNextClick();
    } 
  };

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { 
        control.markAsTouched(); this.markFormTouched(control); 
      } else { 
        control.markAsTouched(); 
      };
    });
  };

  public otpLengthValidator(control: FormControl) {
    if ((control.value) != null && (control.value).length > 6)
      return { 'validLength': true };
  }
}

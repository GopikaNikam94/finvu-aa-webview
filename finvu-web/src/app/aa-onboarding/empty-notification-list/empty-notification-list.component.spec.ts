import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptyNotificationListComponent } from './empty-notification-list.component';

describe('EmptyNotificationListComponent', () => {
  let component: EmptyNotificationListComponent;
  let fixture: ComponentFixture<EmptyNotificationListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmptyNotificationListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyNotificationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-empty-notification-list',
  templateUrl: './empty-notification-list.component.html',
  styleUrls: ['./empty-notification-list.component.css',
              '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class EmptyNotificationListComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  onBackClick() {
    this.router.navigate(['onboarding/webview-login'])
  }

  onOkClick() {
    this.router.navigate(['onboarding/webview-login'])
  }
}

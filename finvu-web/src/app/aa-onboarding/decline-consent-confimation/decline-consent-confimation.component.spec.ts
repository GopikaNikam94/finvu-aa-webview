import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeclineConsentConfimationComponent } from './decline-consent-confimation.component';

describe('DeclineConsentConfimationComponent', () => {
  let component: DeclineConsentConfimationComponent;
  let fixture: ComponentFixture<DeclineConsentConfimationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeclineConsentConfimationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeclineConsentConfimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

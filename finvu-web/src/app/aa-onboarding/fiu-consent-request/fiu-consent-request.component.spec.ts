import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiuConsentRequestComponent } from './fiu-consent-request.component';

describe('FiuConsentRequestComponent', () => {
  let component: FiuConsentRequestComponent;
  let fixture: ComponentFixture<FiuConsentRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiuConsentRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiuConsentRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

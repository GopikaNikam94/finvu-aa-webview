import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Message } from 'src/app/services/message';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MT } from 'src/app/services/message-types';
import { UserLinkedAccountsRequest } from 'src/app/services/payloads/user-linked-accounts';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { ConfigService } from 'src/app/services/config/config-service';
import { WebviewEncryptRequest, EncryptRequest } from 'src/app/services/payloads/webview-encrypt-request';
import { DATA_LIFE_UNIT, RESPONSE_STATUS } from 'src/app/services/util';
import { AccountConsentRequest, AA, ConsentUse, DataDateTimeRange, FIU, User } from 'src/app/services/payloads/account-consent-request';
import { EntityEnum, EntityInfoRequest } from 'src/app/services/payloads/entity-info-request';

@Component({
  selector: 'app-decrypt-consent-request',
  templateUrl: './decrypt-consent-request.component.html',
  styleUrls: ['./decrypt-consent-request.component.css',
  '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class DecryptConsentRequestComponent implements OnInit {

  public webViewDecryptConsentResponse: Observable<Message>; userLinkedAccountResponse: Observable<Message>;
         webViewConsentDetailResponse: Observable<Message>; entityInfoResponse: Observable<Message>;
         webviewEncryptResponse: Observable<Message>; accountConsentResponse: Observable<Message>;
  public webViewDecryptConsentSubscription: Subscription; userLinkedAccountSubscription: Subscription; 
         webViewConsentDetailSubscription: Subscription; entityInfoSubscription: Subscription;
         webviewEncryptSubscription: Subscription; accountConsentSubscription: Subscription;
  public decryptConsentRequestForm: FormGroup;
  public responseStatus: String; responseMessage: String; userId: String; handleStatus: String;
         errorCode: string;
  public linkedAccounts = []; accounts = []; checkBoxList = []; accountTypes:any = [];
         outputArray = []; accountsWithIcons = []; accountInfo = [];
  public fipConsentInfos: any; decryptedResponse: any; dataDateTimeRange: any; consentTypes: any; 
         consentRequestDetails: any; fetchType: any; startTime: any; purposeText: any; fromDate: any; aa: any; 
         toDate: any; text: any; fiu: any; fiuName: any; fiTypes: any; frequency: any; value: any; user: any;
         unit: any; encryptResponse: any; redirectUrl: any; dataFilter: any; dataLife: any; expireTime: any;
         mode: any; signature: any; statusLastupdateTimestamp: any; txnid: any; consentHandle: any; ver: any; 
         consentType: any; fiType: any; consentMode: any; dataLifeUnit: any; dataLifeValue: any; fiuIcon: any;
  public onetime: boolean = true; periodic: boolean = true; linkFlag: any = true; showDiv: boolean = false;
        decryptConsentRequestFlag: any = false;
   
  constructor(public aaWebSocket: AaWebsocketService, public router: Router, 
              public formBuilder:FormBuilder, public configService: ConfigService) {
    console.log("Encrypted Request: ",sessionStorage.getItem("encryptedRequest"));
    console.log("ReqDate: ",sessionStorage.getItem("reqdate"));
    console.log("Encrypted FIU Id: ",sessionStorage.getItem("encryptedFiuId"));
  }

  ngOnInit() {
    this.userId = sessionStorage.getItem("username");
    this.webViewDecryptConsentResponse = this.aaWebSocket.on<Message>(MT.RES.DECRYPTACCOUNTCONSENTREQUEST);
    this.userLinkedAccountResponse = this.aaWebSocket.on<Message>(MT.RES.USERLINKEDACCOUNT);
    this.webViewConsentDetailResponse = this.aaWebSocket.on<Message>(MT.RES.WEBVIEWCONSENTDETAILREQUEST);
    this.webviewEncryptResponse = this.aaWebSocket.on<Message>(MT.RES.WEBVIEWENCRYPTEREQUEST);
    this.accountConsentResponse = this.aaWebSocket.on<Message>(MT.RES.ACCOUNTCONSENTREQUEST);
    this.entityInfoResponse = this.aaWebSocket.on<Message>(MT.RES.ENTITYINFO);     

    this.decryptConsentRequestForm = this.formBuilder.group({
      selectAccount: ['', Validators.compose([Validators.required])]
    });
    this.webviewConsentDetailRequest();
  }

  getIcons() {
    if (sessionStorage.getItem("sid") != null) { 
      let loop = (linkAccount) => {
        const entityInfoRequestPayload: EntityInfoRequest = {
          entityId: linkAccount.fipId,
          entityType: EntityEnum.FIP
        }
        const msgHeader = HeaderBuilder.build(null, MT.REQ.ENTITYINFO);
        const entityInfoRequestMessage: Message = {
          header: msgHeader,
          payload: entityInfoRequestPayload
        };
      
        this.aaWebSocket.send(entityInfoRequestMessage);
        this.entityInfoSubscription = this.entityInfoResponse.subscribe(value => {
          this.entityInfoSubscription.unsubscribe();
            var icon = value.payload['entityIconUri'] == null ? 'assets/images/bank_large_light.png' : value.payload['entityIconUri'].replace('www.', '')  
            this.accountsWithIcons.push({
              icon: {
                icon: icon
              },
              accType: linkAccount.accType,
              maskedAccNumber: linkAccount.maskedAccNumber,
              FIType: linkAccount.FIType,
              fipName: linkAccount.fipName,
              fipId: linkAccount.fipId,
              accRefNumber: linkAccount.accRefNumber,
              linkRefNumber: linkAccount.linkRefNumber
            })
            if(this.accounts.length){
              loop(this.accounts.shift())
            }
        })
      }
      loop(this.accounts.shift());
   } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  userLinkedAccounts() {
    if(sessionStorage.getItem("sid") != null) {
      const userLinkedAccountsRequestPayload: UserLinkedAccountsRequest = {
        userId: this.userId
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.USERLINKEDACCOUNT);

      const userLinkedAccountsRequestMessage: Message = {
        header: msgHeader,
        payload: userLinkedAccountsRequestPayload
      };

      console.log("User Linked Accounts Request :- " + JSON.stringify(userLinkedAccountsRequestMessage));
      this.aaWebSocket.send(userLinkedAccountsRequestMessage);

      this.userLinkedAccountSubscription = this.userLinkedAccountResponse.subscribe(value => {
        
        console.log("User Linked Accounts Response :- " + JSON.stringify(value));
        this.userLinkedAccountSubscription.unsubscribe();

        this.responseStatus = value.payload["status"] ;
        this.responseMessage = value.payload["message"] ;
        
        if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
          this.linkedAccounts = value.payload["LinkedAccounts"];
          console.log("Linked Accounts: ",JSON.stringify(this.linkedAccounts));
            this.linkedAccounts.map(item => {
              return {
                fipId: item.fipId,
                maskedAccNumber: item.maskedAccNumber,
                accRefNumber: item.accRefNumber,
                linkRefNumber: item.linkRefNumber,
                FIType: item.FIType,
                accType: item.accType,
                fipName: item.fipName
              }
            }).forEach(item => this.accounts.push(item));
            console.log("Accounts: ",this.accounts); 

            this.linkedAccounts.map(item => {
              return {
                accType: item.accType
              }
            }).forEach(item => this.accountTypes.push(item.accType));
            this.accountTypes = this.removewithfilter(this.accountTypes).join(', ');
            console.log("Account Types: ",this.accountTypes);
            this.getIcons();
        } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
          alert(this.responseMessage)
        } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
          alert(value.payload["message"])
        } else {
          alert(this.responseMessage)
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated");
    }
  }
  
  removewithfilter(arr) { 
    let outputArray = arr.filter(function(v, i, self) { 
        // It returns the index of the first 
        // instance of each value 
        return i == self.indexOf(v); 
    }); 
    console.log(outputArray);
    return outputArray; 
  } 

  webviewConsentDetailRequest() {
    if(sessionStorage.getItem("sid") != null) {
      const webviewEncryptRequest: WebviewEncryptRequest = {
        encryptedRequest: sessionStorage.getItem("encryptedRequest"),
        requestDate: sessionStorage.getItem("reqdate"),
        encryptedFiuId: sessionStorage.getItem("encryptedFiuId")
      };

      const msgHeader = HeaderBuilder.build(null, MT.REQ.WEBVIEWCONSENTDETAILREQUEST);
  
      const webviewConsentDetailMessage: Message = {
        header: msgHeader,
        payload: webviewEncryptRequest
      };

      console.log("Webview Consent Detail Request: " + JSON.stringify(webviewConsentDetailMessage));
      this.aaWebSocket.send(webviewConsentDetailMessage);
      this.webViewConsentDetailSubscription = this.webViewConsentDetailResponse.subscribe(value => {
        console.log("Webview Consent Detail Response: " + JSON.stringify(value));
        this.webViewConsentDetailSubscription.unsubscribe();
        
        this.responseStatus = value.payload["status"] ;
        this.responseMessage = value.payload["message"] ;

        if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
          this.consentRequestDetails = JSON.stringify(value.payload);
          this.purposeText = value.payload['Purpose'];
          this.aa = value.payload['AA'];
          this.user = value.payload['User'];
          this.text = this.purposeText['text']
          this.dataDateTimeRange = value.payload['DataDateTimeRange'];
          this.toDate = this.dataDateTimeRange['to'];
          this.fromDate = this.dataDateTimeRange['from']
          this.consentTypes = value.payload['consentTypes'].join(', ');
          this.consentType = value.payload['consentTypes'];
          this.fetchType = value.payload['fetchType'];
          this.getFetchType();
          this.startTime = value.payload['startTime'];
          this.fiu = value.payload['FIU'];
          this.fiuName = this.fiu['name'];
          sessionStorage.setItem("fiuName",this.fiuName);
          const entityInfoRequestPayload: EntityInfoRequest = {
            entityId: this.fiu['id'],
            entityType: EntityEnum.FIU
          }
          const msgHeader = HeaderBuilder.build(null, MT.REQ.ENTITYINFO);
          const entityInfoRequestMessage: Message = {
            header: msgHeader,
            payload: entityInfoRequestPayload
          };
        
          this.aaWebSocket.send(entityInfoRequestMessage);
          this.entityInfoSubscription = this.entityInfoResponse.subscribe(value => {
            this.entityInfoSubscription.unsubscribe();
            this.fiuIcon = value.payload['entityIconUri'] == null ? 'assets/images/bank_large_light.png' : value.payload['entityIconUri'].replace('www.', '')  
          })
          this.fiTypes = value.payload['fiTypes'].join(', ');
          this.fiType = value.payload['fiTypes'];
          this.frequency = value.payload['Frequency']
          this.unit = this.frequency['unit']
          this.value = this.frequency['value']
          this.dataFilter = value.payload['DataFilter'];
          this.dataLife = value.payload['DataLife'];
          this.getDataLife(this.dataLife['unit'], this.dataLife['value']);
          this.expireTime = value.payload['expireTime'];
          this.mode = value.payload['mode'];
          this.signature = value.payload['Signature'];
          this.statusLastupdateTimestamp = value.payload['statusLastupdateTimestamp'];
          this.txnid = value.payload['txnid'];
          this.consentHandle = value.payload['ConsentHandle'];
          this.ver = value.payload['ver'];
          this.consentMode = value.payload['mode'];
          sessionStorage.setItem("consentRequestDetails", JSON.stringify(value.payload));
          this.userLinkedAccounts();
        } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
            console.log(this.responseMessage);
            alert("Something went wrong!")
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated");
    }
  }

  getDataLife(unit, value) {
    if(unit == 'INF') {
      this.dataLifeUnit = DATA_LIFE_UNIT.INF
    } else if(unit == 'MONTH') {
      this.dataLifeUnit = DATA_LIFE_UNIT.MONTH
    } else if(unit == 'YEAR') {
      this.dataLifeUnit = DATA_LIFE_UNIT.YEAR
    } else if(unit == 'DAY') {
      this.dataLifeUnit = DATA_LIFE_UNIT.DAY
    }

    if(value > 0) {
      this.dataLifeValue = value
    }
  }

  webviewEncryptRequest(errroCode) {

    if(sessionStorage.getItem("sid") != null) {
     
      const msgHeader = HeaderBuilder.build(null, MT.REQ.WEBVIEWENCRYPTEREQUEST);

      const encryptRequest : EncryptRequest = {
        errorCode: errroCode
      }
      const encryptRequestMessage: Message = {
        header: msgHeader,
        payload: encryptRequest
      };
      console.log("Encrypt Request: ", JSON.stringify(encryptRequestMessage));
      this.aaWebSocket.send(encryptRequestMessage);

      this.webviewEncryptSubscription = this.webviewEncryptResponse.subscribe(value => {
        console.log("Encrypt Response: " + JSON.stringify(value));
        this.redirectUrl = value.payload['redirectUrl'];
        this.webviewEncryptSubscription.unsubscribe();
        sessionStorage.setItem("EncryptResponse", JSON.stringify(value.payload));
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated");
    }
  }

  accountConsentRequest() {
    if(sessionStorage.getItem("sid") != null) {
     
      const AA: AA = {
        id: this.aa.id
      }

      const consentUse: ConsentUse = {
        logUri: "consent_use_loguri",
        count: 2,
        lastUseDateTime: new Date()
      }

      const dataDateTimeRange: DataDateTimeRange = {
        from: this.fromDate,
        to:   this.toDate
      }

      /*Harcoded idTypes value because webViewConsentDetail endpoint response has this value as null asame for consent use*/ 
      const user: User = {
        idTypes: this.user.idTypes,   
        id: this.user.id
      }

      const fiu: FIU = {
        id:   this.fiu['id'],
        name: this.fiu['name']
      }

      const accountConsentRequest: AccountConsentRequest = {

        AA:                             AA,
        consent:                        "Y",
        consentDetailDigitalSignature:  "Signature of AA as defined in W3C standards; Base64 encoded",
        consentTypes:                   this.consentType,
        ConsentUse:                     consentUse,
        createTime:                     new Date(),
        DataDateTimeRange:              dataDateTimeRange,
        DataFilter:                     this.dataFilter,
        DataLife:                       this.dataLife,
        expireTime:                     this.expireTime,
        fetchType:                      this.fetchType,
        fiTypes:                        this.fiType,
        FIPDetails:                     this.accountInfo,
        FIU:                            fiu,
        Frequency:                      this.frequency,
        mode:                           this.mode,
        Purpose:                        this.purposeText,
        Signature:                      this.signature,
        startTime:                      new Date(),
        statusLastupdateTimestamp:      this.statusLastupdateTimestamp,
        txnid:                          this.txnid,
        User:                           user,
        ver:                            this.ver, 
        consentHandleId:                this.consentHandle,
        handleStatus:                   this.handleStatus
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.ACCOUNTCONSENTREQUEST);

      const consentRequestMessage: Message = {
        header: msgHeader,
        payload: accountConsentRequest
      };
      console.log("Consent Request: ", JSON.stringify(consentRequestMessage));
      this.aaWebSocket.send(consentRequestMessage);

      this.accountConsentSubscription = this.accountConsentResponse.subscribe(value => {
      
        console.log("Consent Response: " + JSON.stringify(value));
        this.accountConsentSubscription.unsubscribe();

        this.responseStatus = value.payload["status"] ;
        this.responseMessage = value.payload["message"] ;
        
        if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
          this.fipConsentInfos = value.payload["fipConsentInfos"];
          if(this.handleStatus == this.HANDLE_STATUS.ACCEPT) {
            this.errorCode = "0"
          } else {
            this.errorCode = "1"
          }
          this.webviewEncryptRequest(this.errorCode)
          sessionStorage.setItem("consentStatus", this.handleStatus.toString())
          this.router.navigate(['onboarding/consent-request-result']);
        } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
          console.log("Consent Denied")
          this.errorCode = "1"
          this.webviewEncryptRequest(this.errorCode)
          this.router.navigate(['onboarding']);
        } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
          this.errorCode = "2"
          this.webviewEncryptRequest(this.errorCode)
          this.router.navigate(['onboarding']);
          console.log(value.payload["message"])
        } else {
          alert(this.responseMessage)
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated");
    }
  }

  aggregateAccountByFipId() {
    this.checkBoxList.forEach(linkAccount => {
        let accInfo = this.accountInfo.find(obj => obj.FIP.Id == linkAccount.fipId);
        if(accInfo){
            accInfo.Accounts.push({
              linkRefNumber: linkAccount.linkRefNumber,
              accType: linkAccount.accType,
              accRefNumber: linkAccount.accRefNumber,
              maskedAccNumber: linkAccount.maskedAccNumber,
              FIType: linkAccount.FIType,
              fipId: linkAccount.fipId,
              fipName: linkAccount.fipName
            })
        }else{
            this.accountInfo.push({
                FIP: {
                    id: linkAccount.fipId
                },
                Accounts: [{
                  linkRefNumber: linkAccount.linkRefNumber,
                  accType: linkAccount.accType,
                  accRefNumber: linkAccount.accRefNumber,
                  maskedAccNumber: linkAccount.maskedAccNumber,
                  FIType: linkAccount.FIType,
                  fipId: linkAccount.fipId,
                  fipName: linkAccount.fipName
                }]
            })
        }
    })
    console.log("Accounts:- " +JSON.stringify(this.accountInfo));
    //this.getIcons();
  }

  getFetchType(){
    if(this.fetchType == "ONETIME") {
      this.onetime = true;
      this.periodic = false;
    } else {
      this.periodic = true;
      this.onetime = false;
    }
  }

  onCheckboxChange(account: Account, event:any) {
    if(event.target.checked) {
      this.checkBoxList.push(account);
      sessionStorage.setItem("accounts", JSON.stringify(this.checkBoxList))
    }
    else {
      for(var i=0 ; i < this.checkBoxList.length; i++) {
        if(this.checkBoxList[i] == account){
          this.checkBoxList.splice(i,1);
        }
      }
    }
  }

  isSelected(account: Account){
    return this.checkBoxList.indexOf(account) >= 0;
  }

  onNextClick(){
    if(sessionStorage.getItem("sid") != null) {
      this.aggregateAccountByFipId();
      this.handleStatus = this.HANDLE_STATUS.ACCEPT
      this.accountConsentRequest();
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  onCancelClick() {
    if(sessionStorage.getItem("sid") != null) {
      this.aggregateAccountByFipId()
      this.handleStatus = this.HANDLE_STATUS.REJECT
      this.accountConsentRequest();
    }else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  public selectValidator(control: FormControl) {
    if (typeof this.checkBoxList === 'undefined' || this.checkBoxList === null || this.checkBoxList.length <= 0)
      return { 'atleast': true };
  }

  submitForm() {
    this.markFormTouched(this.decryptConsentRequestForm);
    if (this.decryptConsentRequestForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.decryptConsentRequestForm.getRawValue;
      console.log(JSON.stringify(this.checkBoxList))
      if (this.checkBoxList.length <= 0) {
        alert("Select atleast one account");
         return 
       } 
      this.onNextClick();
    } else {
      console.log("Something went wrong");
    }
  };

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { 
        control.markAsTouched(); this.markFormTouched(control);
      } else { 
        control.markAsTouched(); 
      };
    });
  };

  onLinkAccountClick() {
    this.decryptConsentRequestFlag = true;
    sessionStorage.setItem("decryptConsentRequestFlag", this.decryptConsentRequestFlag)
    this.router.navigate(['onboarding/select-bank']);
    sessionStorage.setItem("linkFlag", this.linkFlag);
  }
  
  private HANDLE_STATUS = {
    ACCEPT: "ACCEPT",
    REJECT: "DENY"
  };
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecryptConsentRequestComponent } from './decrypt-consent-request.component';

describe('DecryptConsentRequestComponent', () => {
  let component: DecryptConsentRequestComponent;
  let fixture: ComponentFixture<DecryptConsentRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecryptConsentRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecryptConsentRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

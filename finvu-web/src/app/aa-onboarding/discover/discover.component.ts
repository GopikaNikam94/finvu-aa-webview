import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { Message } from 'src/app/services/message';
import { Observable, Subscription } from 'rxjs';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { MT } from 'src/app/services/message-types';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { DiscoveryRequest, Customer, FIPDetails, Identifier} from 'src/app/services/payloads/discovery';
import { Accounts} from 'src/app/services/payloads/linking';
import * as uuid from 'uuid';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { UserInfo } from 'src/app/services/payloads/user-info';
import { UserLinkedAccountsRequest } from 'src/app/services/payloads/user-linked-accounts';
import { ConfigService } from 'src/app/services/config/config-service';
import { RESPONSE_STATUS } from 'src/app/services/util';
import { EntityEnum, EntityInfoRequest } from 'src/app/services/payloads/entity-info-request';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.css',
              '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class DiscoverComponent implements OnInit {

  public userLinkedAccountSubscription: Subscription; userInfoSubscription: Subscription;
         subscription: Subscription; entityInfoSubscription: Subscription; 
  public userLinkedAccountResponse: Observable<Message>; userInfoResponse: Observable<Message>;
         discoveryResponse: Observable<Message>; entityInfoResponse: Observable<Message>; 
  public discoverForm: FormGroup;
  public responseStatus: String; responseMessage: String;  mobile: String; handleId: string;
         mobileNumber: String; fipName: String; userId: String;  version: string; username: string
  public userInfo:any; linkedAccounts: any;
  public discoverAccountData = []; discoverAccounts = []; checkBoxList = [];
         linkedAccountsResult = []; accountsWithIcons = [];
  public submitted = false; discoverPage = true; nextButtonDiv = true; routerChanged = true;
         messageDiv = true; icon: any;
  
  constructor(private router:Router, private aaWebSocket: AaWebsocketService , 
              private formBuilder: FormBuilder, private configService: ConfigService) {
      this.version = this.configService.version;
  }

  ngOnInit() {
    
    this.discoverForm = this.formBuilder.group({
      selectAccount: ['', Validators.compose([Validators.required])]
    });

    this.discoveryResponse = this.aaWebSocket.on<Message>(MT.RES.DISCOVERY);
    this.userInfoResponse = this.aaWebSocket.on<Message>(MT.RES.USERINFO);
    this.userLinkedAccountResponse = this.aaWebSocket.on<Message>(MT.RES.USERLINKEDACCOUNT);
    this.entityInfoResponse = this.aaWebSocket.on<Message>(MT.RES.ENTITYINFO);     
	
	  this.handleId = this.configService.userPostfixName;
    
    if(sessionStorage.getItem("sid") != null ) {
      this.userInfoRequest();
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated");
    }
  }

  userInfoRequest(){ 
    if (sessionStorage.getItem("username").includes("@finvu")){
      this.username  = sessionStorage.getItem("username")
    } else {
      this.username  = sessionStorage.getItem("username")+this.handleId;
    }
    const userInfoPayload: UserInfo = {
      userId: this.username
    };
  
    const msgHeader = HeaderBuilder.build(null, MT.REQ.USERINFO);
  
    const userInfoMessage: Message = {
      header: msgHeader,
      payload: userInfoPayload
    };
  
    console.log("User Info Request :- " + JSON.stringify(userInfoMessage));
    this.aaWebSocket.send(userInfoMessage);
    this.userInfoSubscription = this.userInfoResponse.subscribe(value => {
    console.log("UserInfo Response :- " + JSON.stringify(value));
    this.userInfoSubscription.unsubscribe();
    this.responseStatus = value.payload["status"] ;
    this.responseMessage = value.payload["message"] ;
        
    if (this.responseStatus == RESPONSE_STATUS.ACCEPT) {
      this.userInfo = value.payload["UserInfo"];
      this.mobile = this.userInfo.mobileNo
      console.log("Mobile: ", this.mobile)
      this.mobileNumber = this.mobile.substring(6,10)
      sessionStorage.setItem('mobileNumber', this.mobileNumber.toString());
      this.userId = this.userInfo.userId
      console.log("UserId: ", this.userId)
      this.discoveryRequest();
    } else if (this.responseStatus == RESPONSE_STATUS.RETRY) {
        alert(this.responseMessage)
    } else if (this.responseStatus == RESPONSE_STATUS.REJECT) {
        alert(value.payload["message"])
    } else {
        alert(this.responseMessage)
    }
  });
}

  userLinkedAccounts(){
    
    if(sessionStorage.getItem("sid") != null ) {
      const userLinkedAccountsRequestPayload: UserLinkedAccountsRequest = {
        userId: this.userId
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.USERLINKEDACCOUNT);

      const userLinkedAccountsRequestMessage: Message = {
        header: msgHeader,
        payload: userLinkedAccountsRequestPayload
      };

      console.log("User Linked Accounts Request :- " + JSON.stringify(userLinkedAccountsRequestMessage));
      this.aaWebSocket.send(userLinkedAccountsRequestMessage);

      this.userLinkedAccountSubscription = this.userLinkedAccountResponse.subscribe(value => {
        
        console.log("User Linked Accounts Response :- " + JSON.stringify(value));
        this.userLinkedAccountSubscription.unsubscribe();

        this.responseStatus = value.payload["status"] ;
        this.responseMessage = value.payload["message"] ;
        
        if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
          this.linkedAccounts = value.payload["LinkedAccounts"];
          console.log("Linked Accounts: ",JSON.stringify(this.linkedAccounts));

        this.linkedAccounts.map(item => {
          return {
            maskedAccNumber: item.maskedAccNumber,
            accRefNumber: item.accRefNumber,
            FIType: item.FIType,
            accType: item.accType
          }
        }).forEach(item => this.linkedAccountsResult.push(item));

        console.log("Linked Accounts: ",this.linkedAccountsResult);
        } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
          alert(this.responseMessage)
        } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
          console.log(this.responseMessage)
        } else {
          console.log(JSON.stringify(this.responseMessage))
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated");
    }
  }

  discoveryRequest(){
    this.userLinkedAccounts();
    const identifier : Identifier = {
      category: 'STRONG',
      type: 'MOBILE',
      value: this.mobile
    }

    const customer : Customer = {
      id: this.userId,
      Identifiers: [identifier]
    }

    const fipDetails : FIPDetails = {
      fipId: sessionStorage.getItem("fipId"),
      fipName: sessionStorage.getItem("fipName")
    }

    const discoveryRequestPayload: DiscoveryRequest = {
      ver: this.version,
      timestamp: new Date(),
      txnid: uuid.v4(),
      Customer: customer,
      FIPDetails: fipDetails,
      FITypes: JSON.parse(sessionStorage.getItem("fipFitypes"))
    };

    const msgHeader = HeaderBuilder.build(null, MT.REQ.DISCOVERY);

    const discoveryRequestMessage: Message = {
      header: msgHeader,
      payload: discoveryRequestPayload
    };

    console.log("Discovery Request :- " + JSON.stringify(discoveryRequestMessage));
    this.aaWebSocket.send(discoveryRequestMessage);

    if(sessionStorage.getItem("sid") != null ) {
      this.subscription = this.discoveryResponse.subscribe(value => {
        
        console.log("Discovery Response :- " + JSON.stringify(value));
        this.subscription.unsubscribe();

        this.responseStatus = value.payload["status"] ;
        this.responseMessage = value.payload["message"] ;
        
        if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
          this.discoverAccountData = value.payload["DiscoveredAccounts"];
          this.routerChanged = false;
          this.discoverAccountData.map(item => {
            return {
              maskedAccNumber: item.maskedAccNumber,
              accRefNumber: item.accRefNumber,
              FIType: item.FIType,
              accType: item.accType,
              fipName: sessionStorage.getItem("fipName"),
              fipId: sessionStorage.getItem("fipId")
            }
          }).forEach(item => this.discoverAccounts.push(item));
          console.log("Accounts with fip names: ",this.discoverAccounts); 
          this.getIcons();
        } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
          this.routerChanged = true;
          console.log(this.responseMessage)
          if(value.payload["DiscoveredAccounts"] == null) {
            this.router.navigate(['onboarding/discover-account-message']);
          }
        } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
          this.routerChanged = true;
          console.log(value.payload["message"])
          this.router.navigate(['onboarding/discover-account-message']);
        } else {
          console.log(this.responseMessage);
          this.router.navigate(['onboarding/discover-account-message']);
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated");
    }
  }

  getIcons() {
    if (sessionStorage.getItem("sid") != null) { 
      let loop = (linkAccount) => {
        const entityInfoRequestPayload: EntityInfoRequest = {
          entityId: linkAccount.fipId,
          entityType: EntityEnum.FIP
        }
        const msgHeader = HeaderBuilder.build(null, MT.REQ.ENTITYINFO);
        const entityInfoRequestMessage: Message = {
          header: msgHeader,
          payload: entityInfoRequestPayload
        };
      
        this.aaWebSocket.send(entityInfoRequestMessage);
        this.entityInfoSubscription = this.entityInfoResponse.subscribe(value => {
          this.entityInfoSubscription.unsubscribe();
            var icon = value.payload['entityIconUri'] == null ? 'assets/images/bank_large_light.png' : value.payload['entityIconUri'].replace('www.', '')  
            this.accountsWithIcons.push({
              icon: {
                icon: icon
              },
              accType: linkAccount.accType,
              maskedAccNumber: linkAccount.maskedAccNumber,
              FIType: linkAccount.FIType,
              fipName: linkAccount.fipName,
              fipId: linkAccount.fipId,
              accRefNumber: linkAccount.accRefNumber,
              linkRefNumber: linkAccount.linkRefNumber
            })
            if(this.discoverAccounts.length){
              loop(this.discoverAccounts.shift())
            }
        })
      }
      loop(this.discoverAccounts.shift());
   } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  onCheckboxChange(account: Accounts,event:any) {
    if(event.target.checked) {
            this.checkBoxList.push(account);
    }
    else {
        for(var i=0 ; i < this.checkBoxList.length; i++) {
            if(this.checkBoxList[i] == account){
              this.checkBoxList.splice(i,1);
            }
          }
        }
  }

  isSelected(account: Accounts){
    return this.checkBoxList.indexOf(account) >= 0;
  }

  onNextClick(){
    if(sessionStorage.getItem("sid") != null ) {
      console.log("Selected Account(s): ",this.checkBoxList);
      console.log("Linked Account(s): ",this.linkedAccountsResult);
      let result = false;
      let data ;
      let array = [];
      let linkedAccountRefNo = [];
      if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
        sessionStorage.setItem("accounts", JSON.stringify(this.checkBoxList));
        if(this.linkedAccountsResult != null && this.checkBoxList != null) {
          this.checkBoxList.forEach(element => {
            data = this.linkedAccountsResult.find(ob => ob['accRefNumber'] === element.accRefNumber);
            if(data != null){
              array.push(data);
              result = true;
            }
            console.log("Final Data " ,data);
          });

          if(result) {
            array.map(item => {
              return {
                maskedAccNumber: item.maskedAccNumber
              }
            }).forEach(item => linkedAccountRefNo.push(item.maskedAccNumber));

            alert("You have already linked this account(s) "+ JSON.stringify(linkedAccountRefNo).replace(/"/g, ""));
            this.router.navigate(['onboarding/discover']);
          } else {
            this.router.navigate(['onboarding/link']);
          }
        } else {
          this.router.navigate(['onboarding/link']);
        }
      } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
        alert(this.responseMessage)
      } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
        sessionStorage.setItem("accounts", JSON.stringify(this.checkBoxList));
        this.router.navigate(['onboarding/link']);
      } else {
        alert("Something went wrong.")
      }
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated");
    }
  }

  onLinkAccountClick() {
    this.router.navigate(['onboarding/select-bank']);
  }

  onOkClick() {
    if(sessionStorage.getItem('flag')){
      this.router.navigate(['onboarding/webview-login']);
    } else {
      this.router.navigate(['onboarding']);
    }
  }

  public selectValidator(control: FormControl) {
    if (typeof this.checkBoxList === 'undefined' || this.checkBoxList === null || this.checkBoxList.length <= 0)
      return { 'atleast': true };
  }

  submitForm() {
    this.markFormTouched(this.discoverForm);
    if (this.discoverForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.discoverForm.getRawValue;
      if (this.checkBoxList.length <= 0) {
       alert("Select atleast one account");
        return 
      } 
      this.onNextClick();
    } else {
    } 
  };

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { 
        control.markAsTouched(); this.markFormTouched(control);
      } else { 
        control.markAsTouched(); 
      };
    });
  };
}
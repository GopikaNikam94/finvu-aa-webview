import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from 'src/app/services/message';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { MT } from 'src/app/services/message-types';
import { Observable, Subscription } from 'rxjs';
import { LoginRequest } from 'src/app/services/payloads/login';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { UserLinkedAccountsRequest } from 'src/app/services/payloads/user-linked-accounts';
import { UserInfo } from 'src/app/services/payloads/user-info';
import { ConfigService } from 'src/app/services/config/config-service';
import { RESPONSE_STATUS } from 'src/app/services/util';
import { StayAliveRequestService } from 'src/app/services/stay-alive-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css',
    '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class LoginComponent implements OnInit {

  public loginResponse: Observable<Message>; userLinkedAccountResponse: Observable<Message>;
         userInfoResponse: Observable<Message>;
  public userLinkedAccountSubscription: Subscription; userInfoSubscription: Subscription;
         subscription: Subscription;
  public responseStatus: String; responseMessage: String; mobileAuthenticate: string;
         aaId: String; custId: string; username: string; handleId: string;
  public loginForm: FormGroup;
  public userInfo: any; linkedAccounts: any; fiuId: any; reqdate: any; linkFlag: any = true;
  keepAlive: any; hide = true;

  constructor(private router: Router, private aaWebSocket: AaWebsocketService,
    private configService: ConfigService, private formBuilder: FormBuilder,
    private stayAliveRequestService: StayAliveRequestService) {
    this.handleId = this.configService.userPostfixName;
  }

  ngOnInit() {
    this.stayAliveRequestService.callTimeoutMethod();
    sessionStorage.setItem("linkFlag", this.linkFlag);
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._-]{6,30}$')])],
      password: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]{4,4}$')])]
    });
  
    if (sessionStorage.getItem('userIdOrMobileNo').includes("@")) {
      this.custId = sessionStorage.getItem('userIdOrMobileNo').
        substring(0, sessionStorage.getItem('userIdOrMobileNo').indexOf("@"));
      this.username = sessionStorage.getItem('userIdOrMobileNo')
    } else {
      this.custId = sessionStorage.getItem('username').
        substring(0, sessionStorage.getItem('username').indexOf("@"));
      this.username = sessionStorage.getItem('username')
    }
  }

  onLoginClick() {

    sessionStorage.removeItem('sid')
    this.loginResponse = this.aaWebSocket.on<Message>(MT.RES.LOGIN);

    if (this.loginForm.get('username').value.includes('@finvu.in')) {
      alert("username is not valid");
    } else {
      const loginRequestPayload: LoginRequest = {
        username: this.username,
        password: this.loginForm.get('password').value
      };

      if (loginRequestPayload.password == "" || loginRequestPayload.username == ""
        || loginRequestPayload.password == null || loginRequestPayload.username == null) {
        return;
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.LOGIN);

      const loginRequestMessage: Message = {
        header: msgHeader,
        payload: loginRequestPayload
      };

      console.log(loginRequestMessage);
      this.aaWebSocket.send(loginRequestMessage);

      this.subscription = this.loginResponse.subscribe(value => {
        console.log(value);
        this.subscription.unsubscribe();
        sessionStorage.setItem("sid", value.header.sid);
        sessionStorage.setItem("username", this.loginForm.get('username').value)
        sessionStorage.setItem("password", this.loginForm.get('password').value)

        var reponseStatus = value.payload["status"];

        if (reponseStatus == RESPONSE_STATUS.SUCCESS) {
          sessionStorage.setItem("username", this.username)
          this.userInfoRequest();
        } else if (reponseStatus == RESPONSE_STATUS.RETRY) {
          sessionStorage.removeItem("sid");
          sessionStorage.removeItem("password");
          this.onLoginClick();
        } else if (reponseStatus == RESPONSE_STATUS.VERIFY) {
          this.router.navigate(['onboarding/mobile-otp']);
        } else if (reponseStatus == RESPONSE_STATUS.FAILURE || reponseStatus == RESPONSE_STATUS.LOCKED) {
          alert(value.payload["message"]);
          sessionStorage.removeItem("sid");
          sessionStorage.removeItem("password");
          this.loginForm.get("password").reset();
        } else {
          alert("Something went wrong.")
        }
      });
    }
  }

  onForgotPasswordClick(){
    this.router.navigate(['onboarding/forgot-password']);
  }

  userInfoRequest() {

    if (sessionStorage.getItem("sid") != null) {
      this.userInfoResponse = this.aaWebSocket.on<Message>(MT.RES.USERINFO);

      const userInfoPayload: UserInfo = {
        userId: this.username
      };

      const msgHeader = HeaderBuilder.build(null, MT.REQ.USERINFO);

      const userInfoMessage: Message = {
        header: msgHeader,
        payload: userInfoPayload
      };

      console.log("User Info Request :- " + JSON.stringify(userInfoMessage));
      this.aaWebSocket.send(userInfoMessage);
      this.userInfoSubscription = this.userInfoResponse.subscribe(value => {
        console.log("UserInfo Response :- " + JSON.stringify(value));
        this.userInfoSubscription.unsubscribe();
        this.responseStatus = value.payload["status"];
        this.responseMessage = value.payload["message"];

        if (this.responseStatus == RESPONSE_STATUS.ACCEPT) {
          this.userInfo = value.payload["UserInfo"];
          this.mobileAuthenticate = this.userInfo.mobileAuthenticated
          if (this.mobileAuthenticate == 'Y') {
            this.loginForm.reset();
            this.userLinkedAccounts();
          } else if (this.mobileAuthenticate == 'N') {
            alert("Registering Mobile Number");
            this.router.navigate(['onboarding/mobile-otp']);
          } else {
            alert(this.responseMessage)
          }
        } else if (this.responseStatus == RESPONSE_STATUS.REJECT || RESPONSE_STATUS.RETRY) {
          alert(this.responseMessage)
        } else {
          alert(this.responseMessage)
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  userLinkedAccounts() {

    if (sessionStorage.getItem("sid") != null) {
      this.userLinkedAccountResponse = this.aaWebSocket.on<Message>(MT.RES.USERLINKEDACCOUNT);

      const userLinkedAccountsRequestPayload: UserLinkedAccountsRequest = {
        userId: this.userInfo['userId']
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.USERLINKEDACCOUNT);

      const userLinkedAccountsRequestMessage: Message = {
        header: msgHeader,
        payload: userLinkedAccountsRequestPayload
      };

      console.log("User Linked Accounts Request :- " + JSON.stringify(userLinkedAccountsRequestMessage));
      this.aaWebSocket.send(userLinkedAccountsRequestMessage);

      this.userLinkedAccountSubscription = this.userLinkedAccountResponse.subscribe(value => {

        console.log("User Linked Accounts Response :- " + JSON.stringify(value));
        this.userLinkedAccountSubscription.unsubscribe();

        this.responseStatus = value.payload["status"];
        this.responseMessage = value.payload["message"];

        if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
          this.linkedAccounts = value.payload["LinkedAccounts"];
          console.log("Linked Accounts: ", JSON.stringify(this.linkedAccounts));
          sessionStorage.setItem('username', this.userInfo['userId'])
          this.router.navigate(['onboarding/decrypt-consent-request']);
        } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
          alert(this.responseMessage)
        } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
          this.router.navigate(['onboarding/select-bank']);
          console.log(value.payload["message"])
        } else {
          console.log("this.responseMessage: ", this.responseMessage);
          this.router.navigate(['onboarding/linked-account-message']);
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }
}


import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from 'src/app/services/message';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { MT } from 'src/app/services/message-types';
import { Observable, Subscription } from 'rxjs';
import { LoginRequest } from 'src/app/services/payloads/login';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { UserInfo } from 'src/app/services/payloads/user-info';
import { UserLinkedAccountsRequest } from 'src/app/services/payloads/user-linked-accounts';
import { UserOfflineMessages } from 'src/app/services/payloads/user-offine-message';
import { ConfigService } from 'src/app/services/config/config-service';
import { RESPONSE_STATUS } from 'src/app/services/util';

@Component({
  selector: 'app-webview-login',
  templateUrl: './webview-login.component.html',
  styleUrls: ['./webview-login.component.css',
    '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class WebviewLoginComponent implements OnInit {

  public webviewLoginForm: FormGroup
  public subscription: Subscription; userInfoSubscription: Subscription;
  userLinkedAccountSubscription: Subscription; userOfflineMessagesSubscription: Subscription;
  public loginResponse: Observable<Message>; userLinkedAccountResponse: Observable<Message>;
  userInfoResponse: Observable<Message>; userOfflineMessagesResponse: Observable<Message>;
  public responseStatus: String; responseMessage: String; mobileAuthenticate: string;
  currentStatus: string; username: string; handleId: string;
  public userInfo: any; linkedAccounts: any; webviewLogin: any = true; hide = true;
  public offineMessages = [];
  
  constructor(private router: Router, private aaWebSocket: AaWebsocketService, 
    private formBuilder: FormBuilder, private configService: ConfigService) {
    this.webviewLoginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9._-]{6,30}$')])],
      password: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]{4,4}$')])]
    });
    this.handleId = this.configService.userPostfixName;
  }

  ngOnInit() {
    sessionStorage.clear();
    localStorage.clear();
  }

  onLoginClick() {

    sessionStorage.clear();
    this.loginResponse = this.aaWebSocket.on<Message>(MT.RES.LOGIN);

    if (this.webviewLoginForm.get('username').value.includes('@finvu.in')) {
      alert("username is not valid");
    } else {
      this.username = this.webviewLoginForm.get('username').value.includes('@finvu')
        ? this.webviewLoginForm.get('username').value
        : this.webviewLoginForm.get('username').value + this.handleId;

      const loginRequestPayload: LoginRequest = {
        username: this.username,
        password: this.webviewLoginForm.get('password').value
      };

      if (loginRequestPayload.password == "" || loginRequestPayload.username == ""
        || loginRequestPayload.password == null || loginRequestPayload.username == null) {
        return;
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.LOGIN);

      const loginRequestMessage: Message = {
        header: msgHeader,
        payload: loginRequestPayload
      };

      console.log(loginRequestMessage);
      this.aaWebSocket.send(loginRequestMessage);

      this.subscription = this.loginResponse.subscribe(value => {
        console.log(value);
        sessionStorage.setItem("sid", value.header.sid);
        this.subscription.unsubscribe();
        sessionStorage.setItem("username", this.username)
        sessionStorage.setItem("password", this.webviewLoginForm.get('password').value)
        var reponseStatus = value.payload["status"];

        if (reponseStatus == RESPONSE_STATUS.SUCCESS) {
          this.userInfoRequest()
          sessionStorage.setItem("flag", this.webviewLogin);
        } else if (reponseStatus == RESPONSE_STATUS.VERIFY) {
          this.router.navigate(['onboarding/mobile-otp']);
        } else if (reponseStatus == RESPONSE_STATUS.RETRY) {
          this.onLoginClick();
        } else if (reponseStatus == RESPONSE_STATUS.FAILURE 
          || reponseStatus == RESPONSE_STATUS.LOCKED) {
          alert(value.payload["message"])
        } else {
          alert("Something went wrong.")
        }
      });
    }
  }

  userInfoRequest() {

    if (sessionStorage.getItem('sid') != null) {
      this.userInfoResponse = this.aaWebSocket.on<Message>(MT.RES.USERINFO);

      const userInfoPayload: UserInfo = {
        userId: this.username,
      };

      const msgHeader = HeaderBuilder.build(null, MT.REQ.USERINFO);

      const userInfoMessage: Message = {
        header: msgHeader,
        payload: userInfoPayload
      };

      console.log("User Info Request :- " + JSON.stringify(userInfoMessage));
      this.aaWebSocket.send(userInfoMessage);
      this.userInfoSubscription = this.userInfoResponse.subscribe(value => {
        console.log("UserInfo Response :- " + JSON.stringify(value));
        sessionStorage.setItem("sid", value.header.sid);
        this.userInfoSubscription.unsubscribe();
        this.responseStatus = value.payload["status"];
        this.responseMessage = value.payload["message"];

        if (this.responseStatus == RESPONSE_STATUS.ACCEPT) {
          this.userInfo = value.payload["UserInfo"];
          this.mobileAuthenticate = this.userInfo.mobileAuthenticated
          if (this.mobileAuthenticate == 'Y') {
            this.userLinkedAccounts();
          } else if (this.mobileAuthenticate == 'N') {
            alert("Registering Mobile Number");
            this.router.navigate(['onboarding/mobile-otp']);
          } else {
            alert(this.responseMessage)
          }
        } else if (this.responseStatus == RESPONSE_STATUS.REJECT || RESPONSE_STATUS.RETRY) {
          alert(this.responseMessage)
        } else {
          alert(this.responseMessage)
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  userLinkedAccounts() {

    if (sessionStorage.getItem("sid") != null) {
      this.userLinkedAccountResponse = this.aaWebSocket.on<Message>(MT.RES.USERLINKEDACCOUNT);

      const userLinkedAccountsRequestPayload: UserLinkedAccountsRequest = {
        userId: this.userInfo['userId']
      }

      const msgHeader = HeaderBuilder.build(null, MT.REQ.USERLINKEDACCOUNT);

      const userLinkedAccountsRequestMessage: Message = {
        header: msgHeader,
        payload: userLinkedAccountsRequestPayload
      };

      console.log("User Linked Accounts Request :- " + JSON.stringify(userLinkedAccountsRequestMessage));
      this.aaWebSocket.send(userLinkedAccountsRequestMessage);

      this.userLinkedAccountSubscription = this.userLinkedAccountResponse.subscribe(value => {

        console.log("User Linked Accounts Response :- " + JSON.stringify(value));
        this.userLinkedAccountSubscription.unsubscribe();

        this.responseStatus = value.payload["status"];
        this.responseMessage = value.payload["message"];

        if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
          this.linkedAccounts = value.payload["LinkedAccounts"];
          console.log("Linked Accounts: ", JSON.stringify(this.linkedAccounts));
          this.userOffineMessages();
        } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
          alert(this.responseMessage)
        } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
          this.userOffineMessages();
          console.log(value.payload["message"])
        } else {
          console.log("this.responseMessage: ", this.responseMessage);
          this.router.navigate(['onboarding/linked-account-message']);
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  userOffineMessages() {

    this.userOfflineMessagesResponse = this.aaWebSocket.on<Message>(MT.RES.USEROFFLINEMESSAGES);

    const userOfflineMessagesPayload: UserOfflineMessages = {
      userId: sessionStorage.getItem("username")
    };

    const msgHeader = HeaderBuilder.build(null, MT.REQ.USEROFFLINEMESSAGES);

    const userOfflineMessages: Message = {
      header: msgHeader,
      payload: userOfflineMessagesPayload
    };

    console.log("User Offline Messages Request :- " + JSON.stringify(userOfflineMessages));
    this.aaWebSocket.send(userOfflineMessages);
    this.userOfflineMessagesSubscription = this.userOfflineMessagesResponse.subscribe(value => {
      console.log("User Offline Messages Response :- " + JSON.stringify(value));
      this.userOfflineMessagesSubscription.unsubscribe();
      this.responseStatus = value.payload["status"];
      this.responseMessage = value.payload["message"];

      if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
        this.offineMessages = value.payload['offlineMessages'];
        this.offineMessages = this.offineMessages.filter(a => a.messageType !== 'FI-DATA-REQUESTED' 
          && a.messageType != 'PENDING' && a.messageType !== 'FI-DATA-FETCHING' 
          && a.messageType !== 'READY' && a.messageType !== 'REVOKED');
        if (this.offineMessages.find(e => e.messageType === 'CONSENT_REQUESTED')) {
          this.router.navigate(['onboarding/notifications'])
        } else {
          this.router.navigate(['onboarding/empty-notification-list'])
        }
      } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
        console.log(this.responseMessage)
        alert("Something went wrong!")
      } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
        if(this.linkedAccounts == null) {
          this.router.navigate(['onboarding/linked-account-message']);
        } else {
          this.router.navigate(['onboarding/empty-notification-list']);
          console.log(value.payload["message"]);
        }
      } else {
        console.log(this.responseMessage);
        this.router.navigate(['onboarding/empty-notification-list'])
      }
    });
  }

  onRegisterClick() {
    this.router.navigate(['onboarding/webview-register']);
  }

  onForgotPasswordClick(){
    this.router.navigate(['onboarding/forgot-password']);
  }
}
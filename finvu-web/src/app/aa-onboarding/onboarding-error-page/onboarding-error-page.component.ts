import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-onboarding-error-page',
  templateUrl: './onboarding-error-page.component.html',
  styleUrls: ['./onboarding-error-page.component.css',
  '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class OnboardingErrorPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

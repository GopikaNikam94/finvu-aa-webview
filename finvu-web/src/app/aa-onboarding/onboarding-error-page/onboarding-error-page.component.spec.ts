import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnboardingErrorPageComponent } from './onboarding-error-page.component';

describe('OnboardingErrorPageComponent', () => {
  let component: OnboardingErrorPageComponent;
  let fixture: ComponentFixture<OnboardingErrorPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnboardingErrorPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnboardingErrorPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Message } from 'src/app/services/message';
import { MT } from 'src/app/services/message-types';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { MobileVerificationRequest } from 'src/app/services/payloads/mobile-verification';
import { LoginRequest } from 'src/app/services/payloads/login';
import { RESPONSE_STATUS } from 'src/app/services/util';

@Component({
  selector: 'app-mobile-otp',
  templateUrl: './mobile-otp.component.html',
  styleUrls: ['./mobile-otp.component.css',
              '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class MobileOtpComponent implements OnInit {

  public subscription: Subscription; mobileVerificationSubscription: Subscription;
  public loginResponse: Observable<Message>; mobileVerificationResponse: Observable<Message>;
  public mobileOtpForm: FormGroup;
  public submitted = false;
  
  constructor(public router:Router, public aaWebSocket: AaWebsocketService, 
              public formBuilder: FormBuilder) { 
    this.mobileOtpForm = formBuilder.group({
      'mobile': ['', Validators.compose([Validators.required, Validators.pattern('[6-9]\\d{9}')])]
    });
  }

  ngOnInit() {}
 
  onMobileVerification(){

    if(sessionStorage.getItem("sid") != null ) {
      this.mobileVerificationResponse = this.aaWebSocket.on<Message>(MT.RES.MOBILEVERIFICATION);
      const mobileVerificationRequestPayload: MobileVerificationRequest = {
        mobileNum: this.mobileOtpForm.get('mobile').value
      };
      
      if (mobileVerificationRequestPayload.mobileNum == "" || mobileVerificationRequestPayload.mobileNum == null ) {
          return;
      }

      sessionStorage.setItem("mobileNumber",this.mobileOtpForm.get('mobile').value)

      const msgHeader = HeaderBuilder.build(null, MT.REQ.MOBILEVERIFICATION);

      const mobileVerificationRequestMessage: Message = {
        header: msgHeader,
        payload: mobileVerificationRequestPayload
      };
    
      console.log("Mobile Verification Request:", mobileVerificationRequestMessage);

      this.aaWebSocket.send(mobileVerificationRequestMessage);
      this.mobileVerificationSubscription = this.mobileVerificationResponse.subscribe(value => {
        console.log("Mobile Verification Reponse:", value);
        this.mobileVerificationSubscription.unsubscribe();
        var reponseStatus = value.payload["status"] ;
        console.log("reponseStatus: ", reponseStatus);
        this.router.navigate(['onboarding/mobile-verification']);
        if (reponseStatus == RESPONSE_STATUS.SEND){
          alert(value.payload["message"])
        } else if (reponseStatus == RESPONSE_STATUS.RETRY) {
          alert(value.payload["message"])
        } else if(reponseStatus == RESPONSE_STATUS.REJECT) {
          alert(value.payload["message"])
        } else{
          alert("Something went wrong.")
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }
  
  onLogin(){

    sessionStorage.removeItem("sid");
    this.loginResponse = this.aaWebSocket.on<Message>(MT.RES.LOGIN);

    const loginRequestPayload: LoginRequest = {
      username: sessionStorage.getItem("username"),
      password: sessionStorage.getItem("password")
    };

    if (loginRequestPayload.password == "" || loginRequestPayload.username == "" 
            || loginRequestPayload.password == null || loginRequestPayload.username == null) {
        return ;
    }

    const msgHeader = HeaderBuilder.build(null, MT.REQ.LOGIN);

    const loginRequestMessage: Message = {
      header: msgHeader,
      payload: loginRequestPayload
    };

    console.log(loginRequestMessage);
    this.aaWebSocket.send(loginRequestMessage);
    
    this.subscription = this.loginResponse.subscribe(value => {
      console.log(value);
      sessionStorage.setItem("sid", value.header.sid);
      this.subscription.unsubscribe();
       
      var reponseStatus = value.payload["status"] ;
      
      if (reponseStatus == RESPONSE_STATUS.SUCCESS) {
        this.onMobileVerification()
      } else if (reponseStatus == RESPONSE_STATUS.VERIFY){
        this.router.navigate(['onboarding/mobile-otp']);
      } else if (reponseStatus == RESPONSE_STATUS.RETRY){
        this.onLogin();
      } else if (reponseStatus == RESPONSE_STATUS.FAILURE 
        || reponseStatus == RESPONSE_STATUS.LOCKED) {
        alert(value.payload["message"])
      } else {
        alert("Something went wrong.")
      }
    });
  }

  onMobileOtp(){
    //this.onLogin();
    this.onMobileVerification();
  }

  submitForm() {
    this.markFormTouched(this.mobileOtpForm);
    if (this.mobileOtpForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.mobileOtpForm.getRawValue;
      this.onMobileOtp();
    } 
  };

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { 
        control.markAsTouched(); this.markFormTouched(control);
      } else { 
        control.markAsTouched(); 
      };
    });
  };

  keyPress(event: any) {
    const pattern =/[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}

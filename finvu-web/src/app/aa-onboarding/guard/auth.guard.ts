﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Message } from 'src/app/services/message';
import { MT } from 'src/app/services/message-types';
import { Observable, Subscription } from 'rxjs';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { LogoutStatus } from 'src/app/services/logout-status';

@Injectable()
export class AuthGuard implements CanActivate {

  private logoutResponse: Observable<Message>;
  private logoutSubscription: Subscription;
  private showAlert: boolean = true;

  constructor(private router: Router, private aaWebSocket: AaWebsocketService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.showAlert = true;
    this.onLogout();
    console.log("Route data is: " + route.data.currentStatus);
    console.log("Current Status:" + sessionStorage.getItem("currentStatus"));
    if (route.data.currentStatus && route.data.currentStatus == sessionStorage.getItem("currentStatus")) {
      // not logged in so redirect to login page with the return url
      sessionStorage.clear();
      this.router.navigate(['onboarding/webview-login']);
      console.log('Before logging out')
      return false;
    }
    return true;
  }
  
  onLogout() {
    /*If user logged from two places handled that condition here*/
    this.logoutResponse = this.aaWebSocket.on<Message>(MT.RES.LOGOUT);
    sessionStorage.setItem("currentStatus", LogoutStatus.Sucess)
    console.log('Waiting for response from server');
    this.logoutSubscription = this.logoutResponse.subscribe(value => {
      console.log("value: " + JSON.stringify(value));
      this.logoutSubscription.unsubscribe();
      var reponseStatus = value.payload["status"];
      console.log(value.payload["status"])
      if (reponseStatus == "FAILURE") {
        sessionStorage.setItem("currentStatus", reponseStatus);
        console.log("Current Status: " + sessionStorage.getItem("currentStatus"))
      }
      console.log("Router: "+ this.router);
      console.log("ShowAlert: "+ this.showAlert);
      if (this.router.url == '/onboarding/consent-request-result') {
        this.showAlert == false
      } else {
        if (this.showAlert) {
          alert("Your session is no longer valid. Please re-login and try again. Do not refresh the screen.");
          this.showAlert = false;
          this.router.navigate(['onboarding/webview-login']);
        }
      }
    });
  }
}
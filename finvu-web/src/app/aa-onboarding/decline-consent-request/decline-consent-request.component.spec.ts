import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeclineConsentRequestComponent } from './decline-consent-request.component';

describe('DeclineConsentRequestComponent', () => {
  let component: DeclineConsentRequestComponent;
  let fixture: ComponentFixture<DeclineConsentRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeclineConsentRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeclineConsentRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

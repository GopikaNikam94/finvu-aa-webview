import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConsentService } from 'src/app/services/consent-service';
import { UserOfflineMessageService } from 'src/app/services/user-offline-message-service';

@Component({
  selector: 'app-decline-consent-request',
  templateUrl: './decline-consent-request.component.html',
  styleUrls: ['./decline-consent-request.component.css',
  '../../../assets/icofont/css/aa-onboarding-common.scss']
})

export class DeclineConsentRequestComponent implements OnInit {

  public fiuName: any;
  public handleStatus: string;

  constructor(public router: Router, public consentService: ConsentService,
    public userOfflineMessageService: UserOfflineMessageService) { }

  ngOnInit() {
    this.fiuName = sessionStorage.getItem("fiuName")
  }

  okClick(){
    this.handleStatus = this.HANDLE_STATUS.REJECT
    sessionStorage.setItem("consentStatus", this.handleStatus.toString())
    this.consentService.consentRequest(this.handleStatus, JSON.parse(sessionStorage.getItem("ConsentDetails")), null);
    this.userOfflineMessageService.updateUserOffineMessageAck();
    this.router.navigate(['onboarding/decline-consent-confimation']);
  }

  onCancelClick() {
    this.router.navigate(['onboarding/notifications']);
  }

  public HANDLE_STATUS = {
    REJECT: "DENY"
  };
}
          
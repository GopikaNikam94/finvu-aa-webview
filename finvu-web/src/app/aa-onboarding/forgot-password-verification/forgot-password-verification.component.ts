import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { Message } from 'src/app/services/message';
import { Observable, Subscription, timer } from 'rxjs';
import { MT } from 'src/app/services/message-types';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { ForgotPasswordVerifyRequest } from 'src/app/services/payloads/forgot-password-verify';
import { ForgotPasswordRequest } from 'src/app/services/payloads/forgot-password';
import { take, map } from 'rxjs/operators';
import { RESPONSE_STATUS } from 'src/app/services/util';
import { ConfirmedValidator } from 'src/app/services/password-validation';

@Component({
  selector: 'app-forgot-password-verification',
  templateUrl: './forgot-password-verification.component.html',
  styleUrls: ['./forgot-password-verification.component.css',
    '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class ForgotPasswordVerificationComponent implements OnInit {

  public forgotPasswordVerificationForm: FormGroup;
  public mobileNo: String; userId: String; mobileNumber: String; 
  hide = true; confirmHide = true; showlink = true;
  public forgotPasswordVerifyResponse: Observable<Message>;
  public subscription: Subscription; forgotPasswordVerifySubscription: Subscription;
  public forgotPasswordSubscription: Subscription; subscriptionTimer: Subscription;
  public forgotPasswordResponse: Observable<Message>;
  public counter$: Observable<number>; counterTime$: Observable<number>;
  public count = 15;
  
  constructor(private router: Router, private aaWebSocket: AaWebsocketService, formBuilder: FormBuilder) {
    this.forgotPasswordVerificationForm = formBuilder.group({
      otp: ['', Validators.compose([Validators.required, this.noWhitespaceValidator, this.otpLengthValidator, Validators.pattern('^[0-9]+$')])],
      password: ['', Validators.compose([Validators.required, this.passLengthValidator, Validators.pattern('^[0-9]+$')])],
      reEnterPassword: ['', Validators.compose([Validators.required, this.passLengthValidator, Validators.pattern('^[0-9]+$')])],
    }, {
      validator: ConfirmedValidator('password', 'reEnterPassword')
    });
  }

  ngOnInit() {

    this.forgotPasswordResponse = this.aaWebSocket.on<Message>(MT.RES.FORGOTPASSWORD);
    this.forgotPasswordVerifyResponse = this.aaWebSocket.on<Message>(MT.RES.FORGOTPASSWORDVERIFY);

    this.mobileNo = sessionStorage.getItem("forgotMobileNumber");
    this.userId = sessionStorage.getItem("forgotUserId");

    this.mobileNumber = this.mobileNo.substring(6, 10);

    this.setTimer();

  }

  onForgotPasswordVerify() {

    sessionStorage.removeItem("sid");

    const forgotPasswordVerifyRequestPayload: ForgotPasswordVerifyRequest = {
      userId: this.userId,
      mobileNum: this.mobileNo,
      otp: this.forgotPasswordVerificationForm.get('otp').value,
      newPassword: this.forgotPasswordVerificationForm.get('password').value,
      confirmPassword: this.forgotPasswordVerificationForm.get('reEnterPassword').value
    };

    if (forgotPasswordVerifyRequestPayload.mobileNum == "" || forgotPasswordVerifyRequestPayload.mobileNum == null
      || forgotPasswordVerifyRequestPayload.userId == "" || forgotPasswordVerifyRequestPayload.userId == null
      || forgotPasswordVerifyRequestPayload.otp == "" || forgotPasswordVerifyRequestPayload.otp == null
      || forgotPasswordVerifyRequestPayload.newPassword == "" || forgotPasswordVerifyRequestPayload.newPassword == null
      || forgotPasswordVerifyRequestPayload.confirmPassword == "" || forgotPasswordVerifyRequestPayload.confirmPassword == null) {
      return;
    }

    const msgHeader = HeaderBuilder.build(null, MT.REQ.FORGOTPASSWORDVERIFY);

    const forgotPasswordVerifyRequestMessage: Message = {
      header: msgHeader,
      payload: forgotPasswordVerifyRequestPayload
    };

    console.log("forgot password verify Request:", forgotPasswordVerifyRequestMessage);

    this.aaWebSocket.send(forgotPasswordVerifyRequestMessage);
    this.forgotPasswordVerifySubscription = this.forgotPasswordVerifyResponse.subscribe(value => {
      console.log("forgot password verify Reponse:", value);
      this.forgotPasswordVerifySubscription.unsubscribe();
      var reponseStatus = value.payload["status"];
      console.log("reponseStatus: ", reponseStatus);
      if (reponseStatus == RESPONSE_STATUS.ACCEPT) {
        console.log("Your password has been reset successfully")
        this.router.navigate(['onboarding/webview-login']);
      } else if (reponseStatus == RESPONSE_STATUS.REJECT) {
        alert(value.payload["message"])
      } else {
        alert("Something went wrong.")
      }
    });
  }


  onForgotPasswordVerification() {
    
    this.setTimer();

    const forgotPasswordRequestPayload: ForgotPasswordRequest = {
      userId: this.userId,
      mobileNum: this.mobileNo
    };

    const msgHeader = HeaderBuilder.build(null, MT.REQ.FORGOTPASSWORD);

    const forgotPasswordRequestMessage: Message = {
      header: msgHeader,
      payload: forgotPasswordRequestPayload
    };

    console.log("forgot password Request:", forgotPasswordRequestMessage);

    this.aaWebSocket.send(forgotPasswordRequestMessage);
    this.forgotPasswordSubscription = this.forgotPasswordResponse.subscribe(value => {
      console.log("forgot password Reponse:", value);
      this.forgotPasswordSubscription.unsubscribe();
      var reponseStatus = value.payload["status"];
      console.log("reponseStatus: ", reponseStatus);
      if (reponseStatus == RESPONSE_STATUS.SEND) {
        console.log(value.payload["message"])
      } else if (reponseStatus == RESPONSE_STATUS.RETRY) {
        console.log(value.payload["message"])
      } else if (reponseStatus == RESPONSE_STATUS.REJECT) {
        console.log(value.payload["message"])
      } else {
        alert("Something went wrong.")
      }
    });

  }

  public setTimer() {
    this.count = 15;
    this.counterTime$ = timer(0, 1000).pipe(
      take(this.count),
      map(() => --this.count)
    );
    this.showlink = true;
    this.counter$ = timer(15000);
    this.subscriptionTimer = this.counter$.subscribe(() => {
      this.showlink = false;
    });
  }

  onCancel() {
    this.router.navigate(['onboarding/webview-login']);
  }

  submitForm() {
    this.markFormTouched(this.forgotPasswordVerificationForm);
    if (this.forgotPasswordVerificationForm.valid) {
      // You will get form value if your form is valid
      var formValues = this.forgotPasswordVerificationForm.getRawValue;
      this.onForgotPasswordVerify();
    } 
  };

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { 
        control.markAsTouched(); this.markFormTouched(control); 
      } else { 
        control.markAsTouched(); 
      };
    });
  };

  public otpLengthValidator(control: FormControl) {
    if ((control.value) != null && (control.value).length > 6)
      return { 'validLength': true };
  }

  public passLengthValidator(control: FormControl) {
    if ((control.value) != null && (control.value).length > 4)
      return { 'passLength': true };
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
}


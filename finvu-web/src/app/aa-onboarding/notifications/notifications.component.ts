import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Message } from 'src/app/services/message';
import { HeaderBuilder } from 'src/app/services/header-builder';
import { MT } from 'src/app/services/message-types';
import { AaWebsocketService } from 'src/app/services/websocket/aa-websocket.service';
import { Router } from '@angular/router';
import { UserOfflineMessages } from 'src/app/services/payloads/user-offine-message';
import { RESPONSE_STATUS } from 'src/app/services/util';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css',
              '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class NotificationsComponent implements OnInit {

  public userOfflineMessagesResponse: Observable<Message>;
  public userOfflineMessagesSubscription: Subscription;
  public responseStatus: String; responseMessage: String; searchText: String;
  public offineMessages = [];
  public list = false;
  
  constructor(private aaWebSocket: AaWebsocketService, public router: Router) {}

  ngOnInit() {
    this.userOffineMessages();
  }

  userOffineMessages() {

    if(sessionStorage.getItem("sid") != null ) {
      this.userOfflineMessagesResponse = this.aaWebSocket.on<Message>(MT.RES.USEROFFLINEMESSAGES);

      const userOfflineMessagesPayload: UserOfflineMessages = {
        userId: sessionStorage.getItem("username")
      };
  
      const msgHeader = HeaderBuilder.build(null, MT.REQ.USEROFFLINEMESSAGES);
  
      const userOfflineMessages: Message = {
        header: msgHeader,
        payload: userOfflineMessagesPayload
      };
  
      console.log("User Offline Messages Request :- " + JSON.stringify(userOfflineMessages));
      this.aaWebSocket.send(userOfflineMessages);
      this.userOfflineMessagesSubscription = this.userOfflineMessagesResponse.subscribe(value => {
      console.log("User Offline Messages Response:- " + JSON.stringify(value));
  
      if(value == null || value.payload['offlineMessages'] == null){
        this.router.navigate(['onboarding/empty-notification-list'])
      }
      this.userOfflineMessagesSubscription.unsubscribe();
      this.responseStatus = value.payload["status"] ;
      this.responseMessage = value.payload["message"] ;
        
      if (this.responseStatus == RESPONSE_STATUS.SUCCESS) {
          this.offineMessages = value.payload['offlineMessages'];
          this.offineMessages = this.offineMessages.filter(a => a.messageType !== 'FI-DATA-REQUESTED' && a.messageType != 'PENDING'
          && a.messageType !== 'FI-DATA-FETCHING' && a.messageType !== 'READY' && a.messageType !== 'REVOKED');
          this.offineMessages.sort((a,b) => b.messageTimestamp.localeCompare(a.messageTimestamp));
          console.log("Offine Messages :- " + JSON.stringify(this.offineMessages));
          if(this.offineMessages.length == 0){
            this.router.navigate(['onboarding/empty-notification-list'])
          }
      } else if (this.responseStatus == RESPONSE_STATUS.FAILURE) {
          console.log(this.responseMessage)
      } else if (this.responseStatus == RESPONSE_STATUS.RECORD_NOT_FOUND) {
          console.log("Record not found response:- " + JSON.stringify(value));
          this.router.navigate(['onboarding/empty-notification-list'])
      } else {
          console.log(this.responseMessage)
        }
      });
    } else {
      this.router.navigate(['onboarding']);
      console.log("Session id not generated")
    }
  }

  onRowClick(userId, messageId) {
    if(sessionStorage.getItem("sid") != null ) {
      sessionStorage.setItem("messageId", messageId);
      sessionStorage.setItem("username",userId);
      this.router.navigate(['onboarding/fiu-consent-request']);
    } else {
      this.router.navigate(['onboarding/webview-login']);
      console.log("Session id not generated")
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscoverAccountMessageComponent } from './discover-account-message.component';

describe('DiscoverAccountMessageComponent', () => {
  let component: DiscoverAccountMessageComponent;
  let fixture: ComponentFixture<DiscoverAccountMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscoverAccountMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscoverAccountMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

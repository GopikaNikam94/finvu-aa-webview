import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebviewOtpRequestComponent } from './webview-otp-request.component';

describe('WebviewOtpRequestComponent', () => {
  let component: WebviewOtpRequestComponent;
  let fixture: ComponentFixture<WebviewOtpRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebviewOtpRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebviewOtpRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

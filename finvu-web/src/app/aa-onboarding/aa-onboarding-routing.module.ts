import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DiscoverComponent } from './discover/discover.component';
import { LinkingResultComponent } from './linking-result/linking-result.component';
import { LinkComponent } from './link/link.component';
import { SelectBankComponent } from './select-bank/select-bank.component';
import { MobileVerificationComponent } from './mobile-verification/mobile-verification.component';
import { ConsentRequestResultComponent } from './consent-request-result/consent-request-result.component';
import { MobileOtpComponent } from './mobile-otp/mobile-otp.component';
import { WebviewLoginComponent } from './webview-login/webview-login.component';
import { LinkedAccountMessageComponent } from './linked-account-message/linked-account-message.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { FiuConsentRequestComponent } from './fiu-consent-request/fiu-consent-request.component';
import { WebviewRegisterComponent } from './webview-register/webview-register.component';
import { EmptyNotificationListComponent } from './empty-notification-list/empty-notification-list.component';
import { ConsentConfirmationComponent } from './consent-confirmation/consent-confirmation.component';
import { DecryptConsentRequestComponent } from './decrypt-consent-request/decrypt-consent-request.component';
import { DeclineConsentRequestComponent } from './decline-consent-request/decline-consent-request.component';
import { DiscoverAccountMessageComponent } from './discover-account-message/discover-account-message.component';
import { AuthGuard } from 'src/app/aa-onboarding/guard/auth.guard';
import { LogoutStatus } from 'src/app/services/logout-status';
import { DeclineConsentConfimationComponent } from './decline-consent-confimation/decline-consent-confimation.component'
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ForgotPasswordVerificationComponent } from './forgot-password-verification/forgot-password-verification.component';
import { OnboardingComponent } from './onboarding/onboarding.component';
import { WebviewOtpRequestComponent } from './webview-otp-request/webview-otp-request.component';
import { OnboardingErrorPageComponent } from './onboarding-error-page/onboarding-error-page.component';

const routes: Routes = [
  { 
    path: 'onboarding', 
    component: OnboardingComponent
  },
  { 
    path: 'onboarding/login', 
    component: LoginComponent
  },
  { 
    path: 'onboarding/webview-login', 
    component: WebviewLoginComponent
  },
  { 
    path: 'onboarding/register',
    component: RegisterComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] } 
  },
  { 
    path: 'onboarding/discover', 
    component: DiscoverComponent , 
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/linking-result',
    component: LinkingResultComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] } 
  },
  { 
    path: 'onboarding/link', 
    component: LinkComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/select-bank', 
    component: SelectBankComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/mobile-verification', 
    component: MobileVerificationComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/webview-otp-request', 
    component: WebviewOtpRequestComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/consent-request-result', 
    component: ConsentRequestResultComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/mobile-otp', 
    component: MobileOtpComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/linked-account-message', 
    component: LinkedAccountMessageComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/notifications', 
    component: NotificationsComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/fiu-consent-request', 
    component: FiuConsentRequestComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/webview-register', 
    component: WebviewRegisterComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/empty-notification-list', 
    component: EmptyNotificationListComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/consent-confirmation', 
    component: ConsentConfirmationComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/decrypt-consent-request', 
    component: DecryptConsentRequestComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/decline-consent-request', 
    component: DeclineConsentRequestComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/discover-account-message', 
    component: DiscoverAccountMessageComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/decline-consent-confimation', 
    component: DeclineConsentConfimationComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/forgot-password', 
    component: ForgotPasswordComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/forgot-password-verification', 
    component: ForgotPasswordVerificationComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  },
  { 
    path: 'onboarding/onboarding-error-page', 
    component: OnboardingErrorPageComponent,
    canActivate: [AuthGuard],
    data: { userStatus: [LogoutStatus.Failure] }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AaOnboardingRoutingModule { }
